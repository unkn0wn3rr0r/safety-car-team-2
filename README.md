How we work on **Trello**:

https://trello.com/b/YjHqOd9p/car-safety-project-management

# Safety Car - Team 2

Safety Car is insurance-oriented application for the end users. 
It allows simulation of amount and request insurance policies online. 
The application functionalities are related to three different type of users: **public**, **private** and **administrators**.

# How to run this project:
**1.** Clone the repository.

**2.** Put your _spring.datasource.username_ and _spring.datasource.password_ in _application.properties_ file. These are the database credentials in HeidiSQL for example.

**3.** Run the application. The database will be created automatically and the data will be seeded automatically as well. After that you just need to create a registration and you are all set. **Important note:** Every time you start the application the data will be reseeded. So you will need to either to create/register a new account or use one of the following already created accounts:
<pre>
  - <b>Admins:</b>
    - <b>username:</b> kaloyan    <b>password:</b> qweqwe
    - <b>username:</b> strahil    <b>password:</b> qweqwe

  - <b>Regular/Normal users:</b>
    - <b>username:</b> ivan       <b>password:</b> qweqwe
    - <b>username:</b> gosho      <b>password:</b> qweqwe
</pre>

**4.** Thats it. From now on you can use the application with your existing account or create a new one. **Important note:** The first two registered users will have full rights (Regular/Admin/RootAdmin). Each subsequent user will be created as a regular user of the application.

 **Anonymous (public)** users can: 

  - simulate an offer without registration
  - login/register

 **Logged (private)** users can: 

  - simulate an offer
  - they can create a policy after simulating the offer
  - go to their profile page
  - change their password
  - update their information
  - list all of the policies they have been created and see additional details about a particular policy
  - ability to cancel/deny a particular policy


 **Admins (private/administrators)** can : 

  - simulate an offer
  - they can create a policy after simulating the offer
  - go to their profile page
  - change their password
  - update their information
  - list all of the policies they have been created and see additional details about a particular policy
  - ability to cancel/deny a particular policy
  - list all users policies in the application with the ability to sort the policies by: date asc/desc and status of the policy and also see additional information about a particular policy
  - review only the pending policies in the application with the ability to see additional information about a particular policy
  - approve or reject a policy if the policy has pending status, therefore it is not already canceled by the user himself
  - after approval or refusal of the policy request for a given user, an automatic email is sent to the user with additional details and contact information

### Technologies

- *JDK 11*
- *IntelliJ IDEA* - as an IDE during the whole development process
- *Spring Boot/MVC* - as an application framework
- *Spring Security* - for managing users, roles and authentication
- *MySQL/MariaDB* - as a database
- *Hibernate* - storing, accessing, and managing objects in the database
- *Thymeleaf* - as a template engine in order to create an MVC application and to build dynamic web pages
- *Bootstrap* - for interactive and responsive web user interface
- *Swagger* - for design and documentation of our RESTful web services
- *Git* - for source control management and documentation

# Web Application UI

## Home 
![home_page](./UI_images/home_page.png)

## Login 
![login_page](./UI_images/login_page.png)

## Register 
![register_page](./UI_images/register_page.png)

## Admin Page
![admin_page](./UI_images/admin_page.png)

## Admin Policy - Info Page
![policy_info](./UI_images/policy_info.png)

## Admin Policy - Approved
![policy_info_approved](./UI_images/policy_info_approved.png)

## Simulate Offer
![simulate_offer_page](./UI_images/simulate_offer_page.png)

## Finish creation of Policy
![finish_policy](./UI_images/finish_policy.png)

## Profile 
![user_profile_page](./UI_images/user_profile_page.png)

## Profile - All Policies of a User
![personal_policies](./UI_images/personal_policies.png)
