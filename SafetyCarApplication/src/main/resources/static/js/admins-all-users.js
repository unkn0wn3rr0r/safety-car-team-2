const URL_USERS = 'http://localhost:8080/api/v1/users/';
const URL_USER_POLICIES = 'http://localhost:8080/api/v1/policies/';
const SUCCESS_MESSAGE = 'Successfully modified this user!';
const ERROR_MESSAGE = 'Something went wrong!';
const userTableRow = document.getElementById('user-table');
const userPoliciesList = document.getElementById('user-policies');

function showAllUsers() {
    fetch(URL_USERS + 'all')
        .then(validateResponse)
        .then(buildUsersTable)
        .catch(console.log);
}

showAllUsers();

function buildSuccessMessage() {
    const successDiv = document.getElementById('success-message');
    const success = document.createElement('div');
    const successMessage = document.createElement('strong');
    successMessage.className = 'alert alert-success';
    successMessage.innerHTML = SUCCESS_MESSAGE;
    success.appendChild(successMessage);
    successDiv.appendChild(successMessage);
    return {successDiv, successMessage};
}

function processData() {
    showAllUsers();
    const {successDiv, successMessage} = buildSuccessMessage();
    setTimeout(() => successDiv.removeChild(successMessage), 2000);
}

function validateResponse(resp) {
    if (!resp.ok) throw new Error(ERROR_MESSAGE);
    return resp.json();
}

function getRequestInit(event, method) {
    return {
        method: method,
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(event.target.id),
    };
}

function modifyUser(event, method) {
    fetch(URL_USERS + event.target.id, getRequestInit(event, method))
        .then(validateResponse)
        .then(processData)
        .catch(console.log);
}

function processUserPolicyList(policies) {
    if (policies.length === 0) {
        userPoliciesList.innerHTML = `No policies were found for this user 🤷‍♂️` + '<hr>';
    } else {
        buildUserTablePoliciesList(policies);
    }
}

function fetchUserPolicies(event) {
    fetch(URL_USER_POLICIES + event.target.id + '/user')
        .then(validateResponse)
        .then(processUserPolicyList)
        .catch(console.log);
}

function confirmAction(event, action) {
    if (confirm(`Are you sure you want to ${action} this user?`)) {
        switch (action) {
            case 'delete':
                modifyUser(event, 'DELETE');
                break;
            case 'change-role':
                modifyUser(event, 'PUT');
                break;
        }
        return true;
    }
    return false;
}

const events = {
    'btn btn-info': (event) => confirmAction(event, 'change-role'),
    'btn btn-danger delete': (event) => confirmAction(event, 'delete'),
    'btn btn-primary': (event) => fetchUserPolicies(event),
}

function handleEvent() {
    return (event) => events[event.target.className](event);
}

document.body.addEventListener('click', handleEvent());

function createUsersTableBase() {
    return `<table class="table table-hover mb-0">
                 <thead>
                     <tr>
                         <th><b>Username</b></th>
                         <th><b>Email</b></th>
                         <th><b>Full Name</b></th>
                         <th><b>Age</b></th>
                         <th><b>Policies</b></th>
                         <th><b>Role</b></th>
                         <th><b>Action</b></th>
                     <tr>
                  </thead>
                  <tbody>`;
}

function createUsersTableCore(user) {
    return `<tr>
                <td>${user.username}</td>
                <td>${user.email}</td>
                <td>${user.fullName}</td>
                <td>${user.age}</td>
                <td id="${user.id}" class="btn btn-primary">Show Policies</td>
                <td>${!user.role ? 'User' : user.role}</td>
                <td id="${user.id}" class="btn btn-info">Change Role</td>
                <td id="${user.id}" class="btn btn-danger delete${user.role ? ' disabled' : ''}">Delete</td>
            </tr>`;
}

function finishTableBase() {
    return `<tbody><table><hr>`;
}

function buildUsersTable(users) {
    return userTableRow.innerHTML = users.reduce((tableBody, user) => {
        tableBody += createUsersTableCore(user);
        return tableBody;
    }, createUsersTableBase()) + finishTableBase();
}

function createTableBasePolicyList() {
    return `<table class="table table-hover mb-0">
                             <thead>
                                 <tr>
                                    <th><b>Effective Date</b></th>
                                    <th><b>Phone</b></th>
                                    <th><b>Postal Address</b></th>
                                    <th><b>Driver Age</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Car Model</b></th>
                                    <th><b>Cubic Capacity</b></th>
                                    <th><b>Accidents</b></th>
                                    <th><b>Total Premium</b></th>
                                    <th><b>View Details</b></th>
                                <tr>
                            </thead>
                        <tbody>`;
}

function buildUserTablePoliciesList(policies) {
    return userPoliciesList.innerHTML = policies.reduce((tableBody, policy) => {
        tableBody += `<tr>
                          <td>${policy.effectiveDate.join('/')}</td>
                          <td>${policy.phone}</td>
                          <td>${policy.postalAddress}</td>
                          <td>${policy.driverAge}</td>
                          <td>${policy.statusName}</td>
                          <td>${policy.carModel}</td>
                          <td>${policy.cubicCapacity}</td>
                          <td>${policy.hasAccidents}</td>
                          <td>${policy.totalPremium}</td>
                          <td>
                              <a class="btn btn-outline-info btn-circle btn-sm btn-circle" type="submit" href="/admins/policy/${policy.id}/info">
                                <i class="fa fa-info fa-lg" aria-hidden="true"></i>
                              </a>
                          </td>
                     </tr>`;
        return tableBody;
    }, createTableBasePolicyList()) + finishTableBase();
}