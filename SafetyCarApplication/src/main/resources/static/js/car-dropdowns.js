const URL_BRANDS = '/api/v1/brands';
const URL_MODELS = '/api/v1/models/';
const selectBrand = document.getElementById("brand-id");
const selectModel = document.getElementById("model-id");

const generateElements = {
    brand: (brands) => brands.forEach(brand => selectBrand.appendChild(createOption(brand))),
    model: (models) => models.forEach(model => selectModel.appendChild(createOption(model)))
};

fetch(URL_BRANDS)
    .then(resp => resp.json())
    .then(data => generateElements.brand(data))
    .catch(error => console.log(error));

function handleEvent() {
    return function () {
        selectModel.innerHTML = "";
        const modelId = this.value;
        fetch(URL_MODELS + modelId)
            .then(resp => resp.json())
            .then(data => generateElements.model(data))
            .catch(error => console.log(error));
    };
}

selectBrand.addEventListener('change', handleEvent());

function createOption(element) {
    const option = document.createElement("option");
    option.text = element.name;
    option.value = element.id;
    return option;
}