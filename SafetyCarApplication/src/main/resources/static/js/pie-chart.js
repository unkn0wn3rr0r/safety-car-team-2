// Global Options
Chart.defaults.global.defaultFontFamily = 'Lato';
Chart.defaults.global.defaultFontSize = 20;
Chart.defaults.global.defaultFontColor = '#000';

let massPopChart = new Chart(myChart, {
    type: 'pie', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
    data: {
        labels: ['Approved Policies', 'Pending Policies', 'Canceled Policies', 'Rejected Policies'],
        datasets: [{
            data: [
                approved,
                pending,
                rejected,
                canceled,
            ],
            backgroundColor: [
                'rgba(75, 192, 192, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 159, 64, 0.6)',
                'rgba(255, 99, 132, 0.6)'
            ],
            borderWidth: 1,
            borderColor: '#777',
            hoverBorderWidth: 3,
            hoverBorderColor: '#000',
        }]
    },
    options: {
        title: {
            display: true,
            text: 'Policies Status Information',
            fontSize: 25,
        },
        legend: {
            display: true,
            position: 'right',
            labels: {
                fontColor: '#000'
            }
        },
        tooltips: {
            enabled: true
        },
        responsive: false,
    }
});