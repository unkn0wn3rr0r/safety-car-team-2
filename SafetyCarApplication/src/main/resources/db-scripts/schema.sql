create database if not exists safety_car;

use safety_car;

create or replace table base_amount
(
    id          int auto_increment
        primary key,
    cc_min      int    not null,
    cc_max      int    not null,
    car_age_min int    not null,
    car_age_max int    not null,
    base_amount double not null
);

create or replace table brands
(
    id   int auto_increment
        primary key,
    name varchar(100) not null
);

create or replace table models
(
    id       int auto_increment
        primary key,
    name     varchar(100) not null,
    brand_id int          not null,
    constraint models_brands_id_fk
        foreign key (brand_id) references brands (id)
);

create or replace table offers
(
    id                int auto_increment
        primary key,
    cubic_capacity    int(100)   not null,
    registration_date date       not null,
    driver_age        int(100)   not null,
    has_accidents     tinyint(1) not null,
    total_premium     double     not null,
    model_id          int        not null,
    constraint offers_models_id_fk
        foreign key (model_id) references models (id)
);

create or replace table statuses
(
    id   int auto_increment
        primary key,
    name varchar(100) not null,
    constraint statuses_name_uindex
        unique (name)
);

create or replace table user_details
(
    id         int auto_increment
        primary key,
    username   varchar(100) not null,
    email      varchar(100) not null,
    first_name varchar(100) not null,
    last_name  varchar(100) not null,
    age        int(50)      not null,
    enabled    tinyint(1)   null,
    constraint user_details_email_uindex
        unique (email),
    constraint user_details_username_uindex
        unique (username)
);

create or replace table policies
(
    id                             int auto_increment
        primary key,
    effective_date                 date         not null,
    registration_certificate_image longblob     not null,
    postal_address                 varchar(100) not null,
    email                          varchar(100) not null,
    phone                          varchar(10)  not null,
    status_id                      int(100)     not null,
    offer_id                       int(100)     null,
    user_id                        int(100)     not null,
    constraint policies_offers_id_fk
        foreign key (offer_id) references offers (id),
    constraint policies_statuses_id_fk
        foreign key (status_id) references statuses (id),
    constraint policies_user_details_id_fk
        foreign key (user_id) references user_details (id)
);

create or replace index policies_cars_id_fk
    on policies (offer_id);

create or replace table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);

create or replace table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);
