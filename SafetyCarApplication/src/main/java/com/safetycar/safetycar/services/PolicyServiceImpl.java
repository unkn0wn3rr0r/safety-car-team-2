package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.entities.Status;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.repositories.contracts.PolicyRepository;
import com.safetycar.safetycar.services.contracts.PolicyService;
import com.safetycar.safetycar.utils.enums.PolicyStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PolicyServiceImpl implements PolicyService {

    private final PolicyRepository policyRepository;
    private final GenericRepositoryHelper<Policy> policyGenericRepositoryHelper;

    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository,
                             GenericRepositoryHelper<Policy> policyGenericRepositoryHelper) {
        this.policyRepository = policyRepository;
        this.policyGenericRepositoryHelper = policyGenericRepositoryHelper;
    }

    @Override
    public List<Policy> getAll() {
        return policyGenericRepositoryHelper.getAll(Policy.class);
    }

    @Override
    public Policy getById(int id) {
        return policyGenericRepositoryHelper.getById(Policy.class, id);
    }

    @Override
    public Policy create(Policy policy) {
        return policyRepository.create(policy);
    }

    @Override
    public List<Policy> getAllUserPoliciesById(int id) {
        return policyRepository.getAllPoliciesForUserById(id);
    }

    @Override
    public List<Policy> getPendingPolicies() {
        return policyRepository.getPendingPolicies();
    }

    @Override
    public Long getPoliciesSizeByStatusCode(int statusCode) {
        return policyRepository.getPoliciesSizeByStatusCode(statusCode);
    }

    @Override
    public Long countAllUserPoliciesById(int id) {
        return policyRepository.countAllUserPoliciesById(id);
    }

    @Override
    public Long countUserPoliciesByIdAndStatus(int id, String status) {
        return policyRepository.countUserPoliciesByIdAndStatus(id, status);
    }

    @Override
    public Policy processPolicy(Policy policy, PolicyStatus policyStatus) {
        var newStatus = new Status();
        newStatus.setId(policyStatus.getCODE());
        newStatus.setName(policyStatus.name().toLowerCase());
        policy.setStatus(newStatus);
        return policyRepository.update(policy);
    }

    @Override
    public List<Policy> sort(String sort) {
        return policyRepository.sort(sort);
    }
}