package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.dtos.user.UserChangePasswordDto;
import com.safetycar.safetycar.models.dtos.user.UserCreateDto;
import com.safetycar.safetycar.models.entities.UserDetails;

public interface UserAuthorityService {

    void createNewUser(UserCreateDto userCreateDto);

    void deleteUser(UserDetails userDetails, String username, boolean canDeleteUsers);

    void changeUserPassword(UserChangePasswordDto userChangePasswordDto, String username);

    UserDetails changeUserRoles(int id);
}