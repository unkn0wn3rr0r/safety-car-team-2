package com.safetycar.safetycar.services;

import com.safetycar.safetycar.exceptions.PasswordMismatchException;
import com.safetycar.safetycar.exceptions.UnauthorizedRequestException;
import com.safetycar.safetycar.models.dtos.user.UserChangePasswordDto;
import com.safetycar.safetycar.models.dtos.user.UserCreateDto;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.services.contracts.UserAuthorityService;
import com.safetycar.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.safetycar.safetycar.utils.enums.UserRoleType.*;

@Service
public class UserAuthorityServiceImpl implements UserAuthorityService {

    private static final int USERS_COUNT = 2;
    private static final String OLD_PASSWORD_MISMATCH = "Old password is incorrect!";
    private static final String PASSWORDS_MISMATCH = "Passwords do not match!";
    private static final String USER_DELETE_NOT_AUTHORIZED = "Cannot delete this user!";

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsManager userDetailsManager;

    @Autowired
    public UserAuthorityServiceImpl(UserService userService,
                                    PasswordEncoder passwordEncoder,
                                    UserDetailsManager userDetailsManager) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    // create/register new user in User and Authority tables (from Spring Security)
    @Override
    public void createNewUser(UserCreateDto userCreateDto) {
        String username = userCreateDto.getUsername();
        String password = passwordEncoder.encode(userCreateDto.getPassword());

        List<GrantedAuthority> authorities = userService.getAll().size() <= USERS_COUNT
                ? AuthorityUtils.createAuthorityList(ROLE_USER.name(), ROLE_ADMIN.name(), ROLE_ROOT.name())
                : AuthorityUtils.createAuthorityList(ROLE_USER.name());

        var newUser = new User(username, password, authorities);
        userDetailsManager.createUser(newUser);
    }

    // soft delete the user by updating the users enabled property from true to false
    @Override
    public void deleteUser(UserDetails userDetails, String loggedUsername, boolean canDeleteUsers) {
        if (!loggedUsername.equals(userDetails.getUsername()) && !canDeleteUsers) {
            throw new UnauthorizedRequestException(USER_DELETE_NOT_AUTHORIZED);
        }

        String username = userDetails.getUsername();
        var user = userDetailsManager.loadUserByUsername(username);

        String password = user.getPassword();
        var userAuthorities = user.getAuthorities();

        // update enabled to false and update the current user (soft delete)
        var userToDelete = new User(
                username,
                password,
                false,
                true,
                true,
                true,
                userAuthorities
        );
        userDetailsManager.updateUser(userToDelete);
    }

    @Override
    public void changeUserPassword(UserChangePasswordDto userChangePasswordDto, String username) {
        var user = userDetailsManager.loadUserByUsername(username);

        String oldPassword = userChangePasswordDto.getOldPassword();
        String newPassword = userChangePasswordDto.getNewPassword();
        String confirmNewPassword = userChangePasswordDto.getConfirmNewPassword();

        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new PasswordMismatchException(OLD_PASSWORD_MISMATCH);
        }
        if (!newPassword.equals(confirmNewPassword)) {
            throw new PasswordMismatchException(PASSWORDS_MISMATCH);
        }
        String newPasswordEncoded = passwordEncoder.encode(newPassword);
        userDetailsManager.changePassword(oldPassword, newPasswordEncoded);
    }

    @Override
    public UserDetails changeUserRoles(int id) {
        UserDetails userById = userService.getById(id);

        var user = userDetailsManager.loadUserByUsername(userById.getUsername());
        String username = user.getUsername();
        String password = user.getPassword();

        List<GrantedAuthority> authorities = new ArrayList<>(user.getAuthorities());
        if (authorities.size() > 1) {
            authorities.removeIf(grantedAuthority ->
                    ROLE_ADMIN.name().equalsIgnoreCase(grantedAuthority.getAuthority()) ||
                            ROLE_ROOT.name().equalsIgnoreCase(grantedAuthority.getAuthority()));
        } else {
            authorities.addAll(List.of(
                    new SimpleGrantedAuthority(ROLE_ADMIN.name()),
                    new SimpleGrantedAuthority(ROLE_ROOT.name())));
        }

        var userToUpdate = new User(
                username,
                password,
                true,
                true,
                true,
                true,
                authorities
        );
        userDetailsManager.updateUser(userToUpdate);

        return userById;
    }
}