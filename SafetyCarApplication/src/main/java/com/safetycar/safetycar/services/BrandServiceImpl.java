package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.Brand;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    private final GenericRepositoryHelper<Brand> brandRepository;

    @Autowired
    public BrandServiceImpl(GenericRepositoryHelper<Brand> brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public List<Brand> getAll() {
        return brandRepository.getAll(Brand.class);
    }

    @Override
    public Brand getById(int id) {
        return brandRepository.getById(Brand.class, id);
    }
}