package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.UserDetails;

public interface NotificationService {

    void sendPolicyRejectedNotification(UserDetails userDetails, String email);

    void sendPolicyApprovedNotification(UserDetails userDetails, String email);
}