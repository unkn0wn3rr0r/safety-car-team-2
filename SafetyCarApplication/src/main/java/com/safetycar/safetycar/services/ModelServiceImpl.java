package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.Model;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    private final GenericRepositoryHelper<Model> modelRepository;

    @Autowired
    public ModelServiceImpl(GenericRepositoryHelper<Model> modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll() {
        return modelRepository.getAll(Model.class);
    }

    @Override
    public Model getById(int id) {
        return modelRepository.getById(Model.class, id);
    }
}