package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.services.contracts.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Objects;

import static java.lang.String.format;

@Service
public class NotificationServiceImpl implements NotificationService {

    private static final String USERNAME_MAIL_PROPERTY = "spring.mail.username";
    private static final String CONTACT_EMAIL = "safety.car.sofia@gmail.com";
    private static final String POLICY_STATUS_SUBJECT = "Your policy status at Safety Car Insurances";
    private static final String POLICY_REJECTED_MESSAGE_TEXT = "Hello %s. Your policy request was rejected. If you want more information, you can contact us on: %s";
    private static final String POLICY_APPROVED_MESSAGE_TEXT = "Hello %s! Your policy was approved. The policy is active for 1 year from: %s. If you want more information, you can contact us on: %s";

    private final JavaMailSender javaMailSender;
    private final Environment environment;

    @Autowired
    public NotificationServiceImpl(JavaMailSender javaMailSender,
                                   Environment environment) {
        this.javaMailSender = javaMailSender;
        this.environment = environment;
    }

    @Override
    public void sendPolicyRejectedNotification(UserDetails userDetails, String email) throws MailException {
        var mail = new SimpleMailMessage();
        mail.setTo(email);
        mail.setFrom(Objects.requireNonNull(environment.getProperty(USERNAME_MAIL_PROPERTY)));
        mail.setSubject(POLICY_STATUS_SUBJECT);
        mail.setText(format(POLICY_REJECTED_MESSAGE_TEXT, userDetails.getUsername(), CONTACT_EMAIL));
        javaMailSender.send(mail);
    }

    @Override
    public void sendPolicyApprovedNotification(UserDetails userDetails, String email) {
        var mail = new SimpleMailMessage();
        mail.setTo(email);
        mail.setFrom(Objects.requireNonNull(environment.getProperty(USERNAME_MAIL_PROPERTY)));
        mail.setSubject(POLICY_STATUS_SUBJECT);
        mail.setText(format(POLICY_APPROVED_MESSAGE_TEXT, userDetails.getUsername(), LocalDate.now(), CONTACT_EMAIL));
        javaMailSender.send(mail);
    }
}