package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.BaseAmount;
import com.safetycar.safetycar.repositories.contracts.BaseAmountRepository;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.contracts.BaseAmountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class BaseAmountServiceImpl implements BaseAmountService {

    private final BaseAmountRepository baseAmountRepository;
    private final GenericRepositoryHelper<BaseAmount> baseAmountGenericRepositoryHelper;

    @Autowired
    public BaseAmountServiceImpl(BaseAmountRepository baseAmountRepository,
                                 GenericRepositoryHelper<BaseAmount> baseAmountGenericRepositoryHelper) {
        this.baseAmountRepository = baseAmountRepository;
        this.baseAmountGenericRepositoryHelper = baseAmountGenericRepositoryHelper;
    }

    @Override
    public List<BaseAmount> getAll() {
        return baseAmountGenericRepositoryHelper.getAll(BaseAmount.class);
    }

    @Override
    public List<BaseAmount> insertTableData(List<BaseAmount> baseAmountDataList) {
        List<BaseAmount> currentTableData = getAll();

        if (currentTableData.isEmpty()) {
            baseAmountRepository.insertTableData(baseAmountDataList);
            return baseAmountDataList;
        }

        // filter duplicates, take only the new base amount rows and send them to the database
        final Predicate<BaseAmount> thoseWhichAreNotInCurrentTableData = newData -> !currentTableData.contains(newData);

        List<BaseAmount> newDataToInsert = baseAmountDataList.stream()
                .filter(thoseWhichAreNotInCurrentTableData)
                .collect(Collectors.toList());

        baseAmountRepository.insertTableData(newDataToInsert);
        return newDataToInsert;
    }

    @Override
    public BaseAmount search(Integer cubicCapacity, Integer age) {
        return baseAmountRepository.search(cubicCapacity, age);
    }
}