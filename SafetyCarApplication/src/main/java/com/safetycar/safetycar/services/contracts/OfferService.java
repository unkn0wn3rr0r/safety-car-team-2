package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.Offer;

import java.util.List;

public interface OfferService {

    List<Offer> getAll();

    Offer create(Offer offer);

    Offer getById(int id);

    List<Offer> deleteAllUnusedOffers();

    Offer deleteById(int id);
}