package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.BaseAmount;

import java.util.List;

public interface BaseAmountService {

    List<BaseAmount> getAll();

    List<BaseAmount> insertTableData(List<BaseAmount> baseAmountDataList);

    BaseAmount search(Integer cubicCapacity, Integer age);
}