package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.Model;

import java.util.List;

public interface ModelService {

    List<Model> getAll();

    Model getById(int id);
}