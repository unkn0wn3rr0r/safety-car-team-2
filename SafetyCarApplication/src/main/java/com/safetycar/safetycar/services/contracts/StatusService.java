package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);
}
