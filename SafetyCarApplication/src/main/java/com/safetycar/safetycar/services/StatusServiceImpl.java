package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.Status;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final GenericRepositoryHelper<Status> statusRepository;

    @Autowired
    public StatusServiceImpl(GenericRepositoryHelper<Status> statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll() {
        return statusRepository.getAll(Status.class);
    }

    @Override
    public Status getById(int id) {
        return statusRepository.getById(Status.class, id);
    }
}