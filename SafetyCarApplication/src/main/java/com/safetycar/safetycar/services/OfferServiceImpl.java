package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.repositories.contracts.OfferRepository;
import com.safetycar.safetycar.services.contracts.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    private final OfferRepository offerRepository;
    private final GenericRepositoryHelper<Offer> offerGenericRepositoryHelper;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository,
                            GenericRepositoryHelper<Offer> offerGenericRepositoryHelper) {
        this.offerRepository = offerRepository;
        this.offerGenericRepositoryHelper = offerGenericRepositoryHelper;
    }

    @Override
    public List<Offer> getAll() {
        return offerGenericRepositoryHelper.getAll(Offer.class);
    }

    @Override
    public Offer getById(int id) {
        return offerGenericRepositoryHelper.getById(Offer.class, id);
    }

    @Override
    public Offer create(Offer offer) {
        return offerRepository.create(offer);
    }

    @Override
    public List<Offer> deleteAllUnusedOffers() {
        return offerRepository.deleteAllUnusedOffers();
    }

    @Override
    public Offer deleteById(int id) {
        return offerRepository.deleteById(id);
    }
}