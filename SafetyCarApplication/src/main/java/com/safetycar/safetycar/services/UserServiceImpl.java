package com.safetycar.safetycar.services;

import com.safetycar.safetycar.exceptions.EntityDuplicatedException;
import com.safetycar.safetycar.exceptions.UnauthorizedRequestException;
import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.repositories.contracts.UserRepository;
import com.safetycar.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
public class UserServiceImpl implements UserService {

    private static final String USER_USERNAME_DUPLICATION = "User with username: %s, already exists!";
    private static final String USER_EMAIL_DUPLICATION = "User with email: %s, already exists!";
    private static final String USERNAME_CHANGE_NOT_ALLOWED = "Cannot change the username!";
    private static final String USER_DELETE_NOT_AUTHORIZED = "Cannot delete this user!";

    private final UserRepository userRepository;
    private final GenericRepositoryHelper<UserDetails> userDetailsRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           GenericRepositoryHelper<UserDetails> userDetailsRepository) {
        this.userRepository = userRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    public List<UserDetails> getAll() {
        return userDetailsRepository.getAll(UserDetails.class);
    }

    @Override
    public UserDetails getById(int id) {
        return userDetailsRepository.getById(UserDetails.class, id);
    }

    @Override
    public UserDetails create(UserDetails user) {
        String username = user.getUsername();
        String email = user.getEmail();

        if (userRepository.existsByUsername(username)) {
            throw new EntityDuplicatedException(format(USER_USERNAME_DUPLICATION, username));
        }
        if (userRepository.existsByEmail(email)) {
            throw new EntityDuplicatedException(format(USER_EMAIL_DUPLICATION, email));
        }
        return userRepository.create(user);
    }

    @Override
    public UserDetails update(UserDetails user, String username) {
        if (!username.equals(user.getUsername())) {
            throw new UnauthorizedRequestException(USERNAME_CHANGE_NOT_ALLOWED);
        }

        UserDetails userFromDbToBeUpdated = userDetailsRepository.getById(UserDetails.class, user.getId());
        if (!userFromDbToBeUpdated.getUsername().equals(user.getUsername())) {
            throw new UnauthorizedRequestException(USERNAME_CHANGE_NOT_ALLOWED);
        }

        String newUserEmail = user.getEmail();
        if (!userFromDbToBeUpdated.getEmail().equals(newUserEmail)) {
            throwIfEmailExists(user, newUserEmail);
        }

        userFromDbToBeUpdated.setEmail(newUserEmail);
        userFromDbToBeUpdated.setFirstName(user.getFirstName());
        userFromDbToBeUpdated.setLastName(user.getLastName());
        userFromDbToBeUpdated.setAge(user.getAge());
        return userRepository.update(userFromDbToBeUpdated);
    }

    private void throwIfEmailExists(UserDetails user, String newUserEmail) {
        userRepository.findByEmail(newUserEmail).ifPresent(presentUser -> {
            if (presentUser.getId() != user.getId()) {
                throw new EntityDuplicatedException(format(USER_EMAIL_DUPLICATION, newUserEmail));
            }
        });
    }

    @Override
    public UserDetails delete(UserDetails user, String username, boolean canDeleteUsers) {
        if (!username.equals(user.getUsername()) && !canDeleteUsers) {
            throw new UnauthorizedRequestException(USER_DELETE_NOT_AUTHORIZED);
        }
        user.setEnabled(false);
        return userRepository.update(user);
    }

    @Override
    public UserDetails findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<Authority> getAllAdmins() {
        return userRepository.getAllAdmins();
    }
}