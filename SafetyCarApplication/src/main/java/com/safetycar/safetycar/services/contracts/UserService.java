package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;

import java.util.List;

public interface UserService {

    List<UserDetails> getAll();

    UserDetails getById(int id);

    UserDetails create(UserDetails user);

    UserDetails update(UserDetails user, String username);

    UserDetails delete(UserDetails user, String username, boolean canDeleteUsers);

    UserDetails findByUsername(String username);

    List<Authority> getAllAdmins();
}