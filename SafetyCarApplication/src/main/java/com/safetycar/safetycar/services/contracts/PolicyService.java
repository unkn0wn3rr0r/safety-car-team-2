package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.utils.enums.PolicyStatus;

import java.util.List;

public interface PolicyService {

    List<Policy> getAll();

    Policy getById(int id);

    Policy create(Policy policy);

    List<Policy> getAllUserPoliciesById(int id);

    List<Policy> getPendingPolicies();

    Long getPoliciesSizeByStatusCode(int statusCode);

    Long countAllUserPoliciesById(int id);

    Long countUserPoliciesByIdAndStatus(int id, String status);

    Policy processPolicy(Policy policy, PolicyStatus policyStatus);

    List<Policy> sort(String sort);
}