package com.safetycar.safetycar.services.contracts;

import com.safetycar.safetycar.models.entities.Brand;

import java.util.List;

public interface BrandService {

    List<Brand> getAll();

    Brand getById(int id);
}