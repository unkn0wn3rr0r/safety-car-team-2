package com.safetycar.safetycar.utils.helpers;

import com.safetycar.safetycar.models.entities.BaseAmount;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.services.contracts.BaseAmountService;
import com.safetycar.safetycar.utils.helpers.contracts.OfferCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;

import static java.lang.String.format;

@Component
public class OfferCalculatorImpl implements OfferCalculator {

    private final static double ACCIDENT_PERCENTAGE = 1.2;
    private final static int DRIVER_AGE = 25;
    private final static double DRIVER_AGE_PERCENTAGE = 1.05;
    private final static double TAX_PREMIUM_PERCENTAGE = 0.1;

    private final BaseAmountService baseAmountService;

    @Autowired
    public OfferCalculatorImpl(BaseAmountService baseAmountService) {
        this.baseAmountService = baseAmountService;
    }

    @Override
    public double calculateBaseAmount(Offer offer) {
        boolean accidentCoefficient = offer.isHasAccidents();
        int driverAgeCoefficient = offer.getDriverAge();
        int carCapacity = offer.getCubicCapacity();
        int carAge = calculateCarAge(offer.getRegistrationDate());

        BaseAmount baseAmountEntity = baseAmountService.search(carCapacity, carAge);
        double netPremium = baseAmountEntity.getBaseAmount();
        if (accidentCoefficient) {
            netPremium = netPremium * ACCIDENT_PERCENTAGE;
        }
        if (driverAgeCoefficient < DRIVER_AGE) {
            netPremium = netPremium * DRIVER_AGE_PERCENTAGE;
        }

        double taxPremium = netPremium * TAX_PREMIUM_PERCENTAGE;
        String baseAmount = format("%.2f", netPremium + taxPremium);
        return Double.parseDouble(baseAmount);
    }

    private int calculateCarAge(LocalDate registrationDate) {
        LocalDate today = LocalDate.now();
        Period age = Period.between(registrationDate, today);
        return age.getYears();
    }
}