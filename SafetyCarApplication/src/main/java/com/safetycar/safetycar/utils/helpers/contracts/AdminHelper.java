package com.safetycar.safetycar.utils.helpers.contracts;

import org.springframework.security.core.Authentication;

public interface AdminHelper {

    boolean isAdmin(final Authentication authentication);

    boolean isRootAdmin(final Authentication authentication);
}