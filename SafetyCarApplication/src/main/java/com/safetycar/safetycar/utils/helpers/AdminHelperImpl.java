package com.safetycar.safetycar.utils.helpers;

import com.safetycar.safetycar.utils.helpers.contracts.AdminHelper;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import static com.safetycar.safetycar.utils.enums.UserRoleType.ROLE_ADMIN;
import static com.safetycar.safetycar.utils.enums.UserRoleType.ROLE_ROOT;

@Component
public class AdminHelperImpl implements AdminHelper {

    @Override
    public boolean isAdmin(final Authentication authentication) {
        return authentication
                .getAuthorities()
                .stream()
                .anyMatch(role -> ROLE_ADMIN.name().equals(role.getAuthority()));
    }

    @Override
    public boolean isRootAdmin(final Authentication authentication) {
        return authentication
                .getAuthorities()
                .stream()
                .anyMatch(role -> ROLE_ROOT.name().equals(role.getAuthority()));
    }
}