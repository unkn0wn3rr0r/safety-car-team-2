package com.safetycar.safetycar.utils.enums;

public enum PolicyStatus {
    PENDING(4),
    CANCELED(3),
    APPROVED(2),
    REJECTED(1);

    private final int CODE;

    PolicyStatus(int code) {
        CODE = code;
    }

    public int getCODE() {
        return CODE;
    }
}