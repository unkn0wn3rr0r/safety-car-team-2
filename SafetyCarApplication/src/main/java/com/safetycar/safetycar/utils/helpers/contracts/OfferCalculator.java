package com.safetycar.safetycar.utils.helpers.contracts;

import com.safetycar.safetycar.models.entities.Offer;

public interface OfferCalculator {

    double calculateBaseAmount(Offer offer);
}