package com.safetycar.safetycar.utils.constants;

public class SwaggerDocumentationMessages {

    // BASE AMOUNT DOC MESSAGES
    public static final String BASE_AMOUNT_GET_ALL = "Gets the data from the base amount range table";
    public static final String BASE_AMOUNT_INSERT_DATA = "Inserts the data for the base amount range table";
    public static final String BASE_AMOUNT_EXTRA_INFO = "ADMIN/ROOT users only functionality";

    // ADMIN DOC MESSAGES
    public static final String GET_ALL_ADMINS = "Gets all of the admins/root-admins in the application";
    public static final String ADMIN_SORT_POLICIES = "Sorts all of the policies by given criteria";
    public static final String ADMIN_SORT_EXTRA_INFO = "Sorts all policies by: date asc/desc and status (approved, canceled, rejected, pending)";
    public static final String ADMIN_PERMIT_ALL_NOTES = "Only admins have access to this functionality";

    // CAR DOC MESSAGES
    public static final String CARS_MODELS = "Gets all cars models";
    public static final String CARS_BRANDS = "Gets all cars brands";

    // OFFER DOC MESSAGES
    public static final String GET_ALL_OFFERS = "Gets all of the offers available in the application";
    public static final String GET_OFFER_BY_ID = "Gets a particular offer by id";
    public static final String CREATE_OFFER = "Creates a particular offer";
    public static final String DELETE_OFFERS_UNUSED = "Deletes all unused/abandoned offers";
    public static final String DELETE_OFFER_BY_ID = "Deletes a particular offer by id";

    // POLICY DOC MESSAGES
    public static final String POLICIES_GET_ALL = "Gets all policies";
    public static final String GET_POLICY_BY_ID = "Gets a particular policy by id";
    public static final String POLICIES_GET_ALL_BY_ID = "Gets all policies of a particular user by his id";

    // USER DOC MESSAGES
    public static final String USER_GET_ALL = "Gets all of the users in the application";
    public static final String USER_GET_BY_ID = "Gets a particular user by id";
    public static final String USER_CHANGE_ROLES = "Gets a particular user by id and updates his roles/authorities";
    public static final String USER_CREATE = "Creates a particular user";
    public static final String USER_UPDATE = "Updates a particular user";
    public static final String USER_DELETE_BY_ID = "Deletes a particular user";
    public static final String USER_PERMIT_ALL_NOTES = "All users have access to this functionality";
    public static final String USER_PRIVATE_NOTES = "Private users have access to this functionality";
}