package com.safetycar.safetycar.utils.enums;

public enum UserRoleType {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_ROOT
}