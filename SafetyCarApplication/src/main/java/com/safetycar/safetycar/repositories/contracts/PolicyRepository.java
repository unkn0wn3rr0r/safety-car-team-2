package com.safetycar.safetycar.repositories.contracts;

import com.safetycar.safetycar.models.entities.Policy;

import java.util.List;

public interface PolicyRepository {

    Policy create(Policy policy);

    Policy update(Policy policy);

    List<Policy> getAllPoliciesForUserById(int id);

    List<Policy> getPendingPolicies();

    Long getPoliciesSizeByStatusCode(int statusCode);

    Long countAllUserPoliciesById(int id);

    Long countUserPoliciesByIdAndStatus(int id, String status);

    List<Policy> sort(String sort);
}