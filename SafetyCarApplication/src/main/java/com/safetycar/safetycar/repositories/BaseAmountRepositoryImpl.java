package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.BaseAmount;
import com.safetycar.safetycar.repositories.contracts.BaseAmountRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class BaseAmountRepositoryImpl implements BaseAmountRepository {

    private final Logger logger = LoggerFactory.getLogger(BaseAmountRepository.class);

    private static final String BASE_AMOUNT_NOT_FOUND = "There is no base amount found!";
    private static final int JDBC_BATCH_SIZE = 10;

    @PersistenceContext
    private final EntityManager entityManager;
    private final SessionFactory sessionFactory;

    @Autowired
    public BaseAmountRepositoryImpl(EntityManager entityManager,
                                    SessionFactory sessionFactory) {
        this.entityManager = entityManager;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void insertTableData(List<BaseAmount> baseAmountList) {
        Session session = entityManager.unwrap(Session.class);
        String SQL = "INSERT INTO base_amount (base_amount, car_age_max, car_age_min, cc_max, cc_min) VALUES (?, ?, ?, ?, ?)";

        session.setJdbcBatchSize(JDBC_BATCH_SIZE);
        int currentBatchSize = session.getJdbcBatchSize();

        session.doWork(connection -> {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL)) {
                for (int i = 0; i < baseAmountList.size(); i++) {
                    BaseAmount currentBaseAmount = baseAmountList.get(i);
                    preparedStatement.setDouble(1, currentBaseAmount.getBaseAmount());
                    preparedStatement.setInt(2, currentBaseAmount.getCarAgeMax());
                    preparedStatement.setInt(3, currentBaseAmount.getCarAgeMin());
                    preparedStatement.setInt(4, currentBaseAmount.getCubicCapacityMax());
                    preparedStatement.setInt(5, currentBaseAmount.getCubicCapacityMin());
                    preparedStatement.addBatch();
                    // flush a batch of inserts and release memory:
                    if (i > 0 && i % currentBatchSize == 0) {
                        preparedStatement.executeBatch();
                    }
                }
                preparedStatement.executeBatch();
            } catch (SQLException ex) {
                logger.error("An exception occurred in BaseAmountRepository.insertTableData method: {0}", ex);
            }
        });
    }

    @Override
    public BaseAmount search(Integer cubicCapacity, Integer age) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM BaseAmount " +
                    "WHERE (:cubicCapacity >= cubicCapacityMin AND :cubicCapacity <= cubicCapacityMax) " +
                    "AND (:age >= carAgeMin AND :age <= carAgeMax)";

            Query<BaseAmount> query = session.createQuery(SQL, BaseAmount.class)
                    .setParameter("cubicCapacity", cubicCapacity)
                    .setParameter("age", age);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(BASE_AMOUNT_NOT_FOUND);
            }
            return query.list().get(0);
        }
    }
}