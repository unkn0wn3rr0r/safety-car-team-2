package com.safetycar.safetycar.repositories.contracts;

import com.safetycar.safetycar.models.entities.Offer;

import java.util.List;

public interface OfferRepository {

    Offer create(Offer offer);

    List<Offer> deleteAllUnusedOffers();

    Offer deleteById(int id);
}