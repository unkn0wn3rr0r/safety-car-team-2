package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final String USER_BY_USERNAME_NOT_FOUND = "User with username %s, not found!";

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    // if there is user with that username already (list is not empty), then return true
    @Override
    public boolean existsByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM UserDetails AS ud WHERE ud.username =: username";

            Query<UserDetails> userByUsername = session.createQuery(SQL, UserDetails.class);
            userByUsername.setParameter("username", username);

            return !userByUsername.list().isEmpty();
        }
    }

    // if there is user with that email already (list is not empty), then return true
    @Override
    public boolean existsByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM UserDetails AS ud WHERE ud.email =: email";

            Query<UserDetails> userByEmail = session.createQuery(SQL, UserDetails.class);
            userByEmail.setParameter("email", email);

            return !userByEmail.list().isEmpty();
        }
    }

    @Override
    public Optional<UserDetails> findByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM UserDetails AS ud WHERE ud.email =: email";

            Query<UserDetails> userByEmail = session.createQuery(SQL, UserDetails.class);
            userByEmail.setParameter("email", email);

            return userByEmail.list().isEmpty()
                    ? Optional.empty()
                    : Optional.of(userByEmail.list().get(0));
        }
    }

    @Override
    public UserDetails findByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM UserDetails AS ud WHERE ud.username =: username";

            Query<UserDetails> userByUsername = session.createQuery(SQL, UserDetails.class);
            userByUsername.setParameter("username", username);

            if (userByUsername.list().isEmpty()) {
                throw new EntityNotFoundException(String.format(USER_BY_USERNAME_NOT_FOUND, username));
            }
            return userByUsername.list().get(0);
        }
    }

    @Override
    public List<Authority> getAllAdmins() {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "" +
                    "SELECT NEW com.safetycar.safetycar.models.entities.Authority(a.username, a.authority) " +
                    "FROM Authority AS a " +
                    "WHERE a.authority IN ('ROLE_ADMIN')";

            Query<Authority> admins = session.createQuery(SQL, Authority.class);
            return admins.list();
        }
    }

    @Override
    public UserDetails create(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
            return user;
        }
    }

    @Override
    public UserDetails update(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
            return user;
        }
    }
}