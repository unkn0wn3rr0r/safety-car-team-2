package com.safetycar.safetycar.repositories.contracts;

import com.safetycar.safetycar.models.entities.BaseAmount;

import java.util.List;

public interface BaseAmountRepository {

    void insertTableData(List<BaseAmount> baseAmountDataList);

    BaseAmount search(Integer cubicCapacity, Integer age);
}