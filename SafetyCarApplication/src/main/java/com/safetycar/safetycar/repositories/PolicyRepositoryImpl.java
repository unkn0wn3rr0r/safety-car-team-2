package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.BadRequestException;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.repositories.contracts.PolicyRepository;
import com.safetycar.safetycar.utils.enums.PolicyStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.lang.String.format;

@Repository
public class PolicyRepositoryImpl implements PolicyRepository {

    private static final int POSITION = 1;
    private static final String BAD_SORT_REQUEST = "Bad sort criteria -> %s. You can try with one of the following: %s";

    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Policy create(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policy);
            return policy;
        }
    }

    @Override
    public Policy update(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(policy);
            transaction.commit();
            return policy;
        }
    }

    @Override
    public List<Policy> getAllPoliciesForUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM Policy AS p WHERE p.userDetails.id =:id ORDER BY p.effectiveDate DESC";

            Query<Policy> allUserPolicies = session.createQuery(SQL, Policy.class);
            allUserPolicies.setParameter("id", id);

            return allUserPolicies.list();
        }
    }

    @Override
    public List<Policy> getPendingPolicies() {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "FROM Policy AS p WHERE p.status.id =:id";

            Query<Policy> pendingPolicies = session.createQuery(SQL, Policy.class);
            pendingPolicies.setParameter("id", PolicyStatus.PENDING.getCODE());

            return pendingPolicies.list();
        }
    }

    @Override
    public Long getPoliciesSizeByStatusCode(int statusCode) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "SELECT COUNT(*) FROM Policy AS p WHERE p.status.id =:statusCode";

            Query<Long> policiesSizeByStatusCode = session.createQuery(SQL, Long.class);
            policiesSizeByStatusCode.setParameter("statusCode", statusCode);

            if (policiesSizeByStatusCode.list().isEmpty()) {
                return 0L;
            }
            return policiesSizeByStatusCode.list().get(0);
        }
    }

    @Override
    public Long countAllUserPoliciesById(int id) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "SELECT COUNT(*) FROM Policy AS p WHERE p.userDetails.id =:id";

            Query<Long> allUserPoliciesCount = session.createQuery(SQL, Long.class);
            allUserPoliciesCount.setParameter("id", id);

            if (allUserPoliciesCount.list().isEmpty()) {
                return 0L;
            }
            return allUserPoliciesCount.list().get(0);
        }
    }

    @Override
    public Long countUserPoliciesByIdAndStatus(int id, String status) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "SELECT COUNT(*) FROM Policy AS p JOIN Status AS s ON s.id = p.status.id " +
                    "WHERE p.userDetails.id =:id AND s.name =:status";

            Query<Long> allUserPoliciesCount = session.createQuery(SQL, Long.class);
            allUserPoliciesCount.setParameter("id", id);
            allUserPoliciesCount.setParameter("status", status);

            if (allUserPoliciesCount.list().isEmpty()) {
                return 0L;
            }
            return allUserPoliciesCount.list().get(0);
        }
    }

    @Override
    public List<Policy> sort(final String sort) {
        var policySortType = getPolicySortType(sort);

        try (Session session = sessionFactory.openSession()) {
            final Map<PolicySortType, Supplier<List<Policy>>> sortedPolicies = new LinkedHashMap<>() {{
                put(PolicySortType.DATE_ASC, () -> session.createNativeQuery(PolicySortType.DATE_ASC.getSQL(), Policy.class).list());
                put(PolicySortType.DATE_DESC, () -> session.createNativeQuery(PolicySortType.DATE_DESC.getSQL(), Policy.class).list());
                put(PolicySortType.PENDING, () -> session.createNativeQuery(PolicySortType.PENDING.getSQL(), Policy.class)
                        .setParameter(POSITION, PolicyStatus.PENDING.getCODE()).list());
                put(PolicySortType.CANCELED, () -> session.createNativeQuery(PolicySortType.CANCELED.getSQL(), Policy.class)
                        .setParameter(POSITION, PolicyStatus.CANCELED.getCODE()).list());
                put(PolicySortType.APPROVED, () -> session.createNativeQuery(PolicySortType.APPROVED.getSQL(), Policy.class)
                        .setParameter(POSITION, PolicyStatus.APPROVED.getCODE()).list());
                put(PolicySortType.REJECTED, () -> session.createNativeQuery(PolicySortType.REJECTED.getSQL(), Policy.class)
                        .setParameter(POSITION, PolicyStatus.REJECTED.getCODE()).list());
            }};
            return sortedPolicies.get(policySortType).get();
        }
    }

    private PolicySortType getPolicySortType(final String sort) {
        return Arrays.stream(PolicySortType.values())
                .filter(sortType -> sortType.name().equalsIgnoreCase(sort))
                .findFirst()
                .orElseThrow(() -> new BadRequestException(format(BAD_SORT_REQUEST, sort, Arrays.toString(PolicySortType.values()))));
    }

    private enum PolicySortType {
        DATE_ASC("SELECT * FROM safety_car.policies AS p ORDER BY p.effective_date ASC"),
        DATE_DESC("SELECT * FROM safety_car.policies AS p ORDER BY p.effective_date DESC"),
        PENDING("SELECT * FROM safety_car.policies AS p ORDER BY p.status_id = ?1 DESC"),
        CANCELED("SELECT * FROM safety_car.policies AS p ORDER BY p.status_id = ?1 DESC"),
        APPROVED("SELECT * FROM safety_car.policies AS p ORDER BY p.status_id = ?1 DESC"),
        REJECTED("SELECT * FROM safety_car.policies AS p ORDER BY p.status_id = ?1 DESC");

        private final String SQL;

        PolicySortType(String sql) {
            SQL = sql;
        }

        public String getSQL() {
            return SQL;
        }
    }
}