package com.safetycar.safetycar.repositories.contracts;

import java.util.List;

public interface GenericRepositoryHelper<T> {

    List<T> getAll(final Class<T> entity);

    T getById(final Class<T> entity, final int id);
}