package com.safetycar.safetycar.repositories.contracts;

import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    UserDetails create(UserDetails user);

    UserDetails update(UserDetails user);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    Optional<UserDetails> findByEmail(String email);

    UserDetails findByUsername(String username);

    List<Authority> getAllAdmins();
}