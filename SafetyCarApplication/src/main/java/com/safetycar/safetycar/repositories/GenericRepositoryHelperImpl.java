package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static java.lang.String.format;

@Repository
public class GenericRepositoryHelperImpl<T> implements GenericRepositoryHelper<T> {

    private static final String ENTITY_NOT_FOUND = "%s with id: %d, not found!";

    private final SessionFactory sessionFactory;

    @Autowired
    public GenericRepositoryHelperImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<T> getAll(final Class<T> entity) {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = createGetAllSqlQuery(entity);
            Query<T> allEntities = session.createQuery(SQL, entity);
            return allEntities.list();
        }
    }

    @Override
    public T getById(final Class<T> entity, final int id) {
        try (Session session = sessionFactory.openSession()) {
            Optional<T> entityById = Optional.ofNullable(session.get(entity, id));
            if (isUser(entity)) {
                return entityById.filter(user -> ((UserDetails) user).getEnabled()).orElseThrow(notFoundException(entity, id));
            }
            return entityById.orElseThrow(notFoundException(entity, id));
        }
    }

    private String createGetAllSqlQuery(final Class<T> entity) {
        if (isUser(entity)) {
            return format("FROM %s AS ud WHERE ud.enabled = true", entity.getSimpleName());
        }
        return format("FROM %s", entity.getSimpleName());
    }

    private boolean isUser(final Class<T> entity) {
        return entity.getSimpleName().equals(UserDetails.class.getSimpleName());
    }

    private Supplier<EntityNotFoundException> notFoundException(final Class<T> entity, final int id) {
        return () -> new EntityNotFoundException(format(ENTITY_NOT_FOUND, entity.getSimpleName(), id));
    }
}