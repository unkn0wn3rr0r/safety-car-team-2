package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.repositories.contracts.OfferRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class OfferRepositoryImpl implements OfferRepository {

    private final Logger logger = LoggerFactory.getLogger(OfferRepository.class);

    private final static String OFFER_NOT_FOUND = "Offer with id %d, not found!";
    private final static String UNUSED_OFFERS_NOT_FOUND = "There are no unused offers to delete, size is: ";

    private final SessionFactory sessionFactory;

    @Autowired
    public OfferRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Offer create(Offer offer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(offer);
            return offer;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Offer> deleteAllUnusedOffers() {
        try (Session session = sessionFactory.openSession()) {
            final String SQL = "" +
                    "FROM Policy AS p " +
                    "RIGHT JOIN Offer AS o ON o.id = p.offer.id " +
                    "WHERE p.id IS NULL";

            // Hibernate cannot createQuery from more than 1 entity, so we are hacking it to get raw objects instead
            List<Object[]> rawOffers = session.createQuery(SQL).list();
            if (rawOffers.isEmpty()) {
                logger.warn("===> {} {}", UNUSED_OFFERS_NOT_FOUND, 0);
                throw new EntityNotFoundException(UNUSED_OFFERS_NOT_FOUND + 0);
            }

            List<Offer> offers = getOffers(rawOffers);
            deleteOffers(session, offers);
            return offers;
        }
    }

    private void deleteOffers(Session session, List<Offer> offers) {
        Transaction transaction = session.beginTransaction();
        logger.info("===> Starting deletion of unused offers...");
        for (Offer offer : offers) {
            logger.info("===> Delete unused offer: {}", offer);
            session.delete(offer);
        }
        logger.info("===> Deletion of unused offers completed!");
        transaction.commit();
    }

    private List<Offer> getOffers(List<Object[]> rawOffers) {
        return rawOffers.stream()
                .filter(obj -> obj[1] instanceof Offer)
                .map(obj -> (Offer) obj[1])
                .collect(Collectors.toList());
    }

    @Override
    public Offer deleteById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Offer offer = Optional.ofNullable(session.get(Offer.class, id))
                    .orElseThrow(() -> new EntityNotFoundException(String.format(OFFER_NOT_FOUND, id)));

            Transaction transaction = session.beginTransaction();
            session.delete(offer);
            transaction.commit();

            return offer;
        }
    }
}