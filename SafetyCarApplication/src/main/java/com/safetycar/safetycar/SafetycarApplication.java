package com.safetycar.safetycar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class SafetycarApplication {

    public static void main(String[] args) {
        SpringApplication.run(SafetycarApplication.class, args);
    }
}