package com.safetycar.safetycar.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class HibernateConfiguration {

    // trace, debug, info, warn, error
    private final Logger logger = LoggerFactory.getLogger(HibernateConfiguration.class);

    private final String dbDriverClassName;
    private final String dbUrl;
    private final String dbUsername;
    private final String dbPassword;

    public HibernateConfiguration(Environment environment) {
        dbDriverClassName = environment.getProperty("spring.datasource.driverClassName");
        dbUrl = environment.getProperty("spring.datasource.url");
        dbUsername = environment.getProperty("spring.datasource.username");
        dbPassword = environment.getProperty("spring.datasource.password");
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        var sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.safetycar.safetycar.models.entities");
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        var dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dbDriverClassName);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }

    @Bean
    public DataSourceInitializer seedData(DataSource dataSource) {
        var resourceDatabasePopulator = getResourceDatabasePopulator();
        return getDataSourceInitializer(dataSource, resourceDatabasePopulator);
    }

    private ResourceDatabasePopulator getResourceDatabasePopulator() {
        var resourceDatabasePopulator = new ResourceDatabasePopulator();
        var schemaScript = getScript("/db-scripts/schema.sql");
        var dataScript = getScript("/db-scripts/data.sql");
        resourceDatabasePopulator.setContinueOnError(true);
        resourceDatabasePopulator.addScript(schemaScript);
        resourceDatabasePopulator.addScript(dataScript);
        logger.info("===> Creating schema from {} .......", schemaScript.getFilename());
        logger.info("===> Schema from {} was created successfully!", schemaScript.getFilename());
        logger.info("===> Seeding data from {} .......", dataScript.getFilename());
        logger.info("===> Data from {} was seeded successfully!", dataScript.getFilename());
        return resourceDatabasePopulator;
    }

    private DataSourceInitializer getDataSourceInitializer(DataSource dataSource, ResourceDatabasePopulator resourceDatabasePopulator) {
        var dataSourceInitializer = new DataSourceInitializer();
        dataSourceInitializer.setDataSource(dataSource);
        dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
        return dataSourceInitializer;
    }

    private ClassPathResource getScript(String path) {
        return new ClassPathResource(path);
    }

    private Properties hibernateProperties() {
        var properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return properties;
    }
}