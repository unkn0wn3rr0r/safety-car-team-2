package com.safetycar.safetycar.configuration;

import com.safetycar.safetycar.filters.LoginRegisterPageFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.ui.DefaultLoginPageGeneratingFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;

    @Autowired
    public SecurityConfiguration(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    // auth manager builder sets the logic for users, passwords and roles
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        var jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }

    // http configuration which sets the application security behaviour
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(new LoginRegisterPageFilter(), DefaultLoginPageGeneratingFilter.class);

        http.csrf().disable()
                .cors().disable()
                .authorizeRequests()
                .antMatchers("/static/**", "/", "/index", "/login", "/register", "/offers/**", "/about").permitAll()
                .antMatchers("/api/v1/base-amount/**", "/api/v1/admins/**").hasAnyRole("ADMIN", "ROOT")
                .antMatchers("/api/v1/models/**", "/api/v1/brands", "/api/v1/users").permitAll()
                .antMatchers("/home").hasAnyRole("USER", "ADMIN", "ROOT")
                .antMatchers("/admins/**").hasAnyRole("ADMIN", "ROOT")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticate").permitAll()
                .defaultSuccessUrl("/", true)
                .and()
                .logout().logoutSuccessUrl("/login?logout").permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");
    }
}