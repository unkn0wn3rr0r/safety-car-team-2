package com.safetycar.safetycar.exceptions;

public class PasswordMismatchException extends RuntimeException {

    public PasswordMismatchException() {
        super();
    }

    public PasswordMismatchException(String message) {
        super(message);
    }
}