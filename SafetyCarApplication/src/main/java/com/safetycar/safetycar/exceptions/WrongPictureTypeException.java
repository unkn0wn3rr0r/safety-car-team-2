package com.safetycar.safetycar.exceptions;

public class WrongPictureTypeException extends RuntimeException {

    public WrongPictureTypeException() {
        super();
    }

    public WrongPictureTypeException(String message) {
        super(message);
    }
}