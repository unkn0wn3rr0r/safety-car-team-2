package com.safetycar.safetycar.exceptions;

public class IOPictureException extends RuntimeException{

    public IOPictureException() {
        super();
    }

    public IOPictureException(String message) {
        super(message);
    }
}