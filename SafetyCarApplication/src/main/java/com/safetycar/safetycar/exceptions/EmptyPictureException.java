package com.safetycar.safetycar.exceptions;

public class EmptyPictureException extends RuntimeException {

    public EmptyPictureException() {
        super();
    }

    public EmptyPictureException(String message) {
        super(message);
    }
}