package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.services.contracts.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
@RequestMapping("/")
public class HomeController {

    private final OfferService offerService;

    @Autowired
    public HomeController(OfferService offerService) {
        this.offerService = offerService;
    }

    @GetMapping
    public String showHomePage(Model model,
                               Principal principal,
                               HttpSession httpSession) {
        if (principal != null) {
            model.addAttribute("currentUser", principal.getName());
            Offer theOffer = (Offer) httpSession.getAttribute("theOffer");
            // if there is offer in the current session for the current user
            if (theOffer != null) {
                try {
                    // if the offer is not saved in db, because user went back after creating the offer, catch exception
                    offerService.getById(theOffer.getId());
                } catch (EntityNotFoundException e) {
                    httpSession.removeAttribute("theOffer");
                    return "common/home";
                }
                // if the offer is saved in db and user is logged, redirect him/her to policies page
                return "redirect:/policies";
            }
            // if no offer in the current session go to home page
            return "common/home";
        }
        return "common/index";
    }

    @GetMapping("/about")
    public String showAboutPage() {
        return "common/about";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "common/access-denied";
    }
}