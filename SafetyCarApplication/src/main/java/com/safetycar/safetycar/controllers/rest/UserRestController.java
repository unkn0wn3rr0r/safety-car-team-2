package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.dtos.user.UserCreateDto;
import com.safetycar.safetycar.models.dtos.user.UserDto;
import com.safetycar.safetycar.models.dtos.user.UserUpdateDto;
import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.models.mappers.contracts.UserMapper;
import com.safetycar.safetycar.services.contracts.UserAuthorityService;
import com.safetycar.safetycar.services.contracts.UserService;
import com.safetycar.safetycar.utils.helpers.contracts.AdminHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.*;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {

    private static final String EDIT_NOT_AUTHORIZED = "Not authorized to edit the user!";

    private final UserMapper userMapper;
    private final UserService userService;
    private final AdminHelper adminHelper;
    private final UserAuthorityService userAuthorityService;

    @Autowired
    public UserRestController(UserMapper userMapper,
                              UserService userService,
                              AdminHelper adminHelper,
                              UserAuthorityService userAuthorityService) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.adminHelper = adminHelper;
        this.userAuthorityService = userAuthorityService;
    }

    @ApiOperation(value = USER_GET_ALL, notes = USER_PERMIT_ALL_NOTES)
    @GetMapping
    public List<UserDetails> getAll() {
        return userService.getAll();
    }

    @ApiOperation(value = USER_GET_ALL, notes = ADMIN_PERMIT_ALL_NOTES)
    @GetMapping("/all")
    public List<UserDto> getAllUsersAndAdmins() {
        List<Authority> admins = userService.getAllAdmins();
        return userService
                .getAll()
                .stream()
                .map(user -> userMapper.userDetailsAndAdminsToUserDto(admins, user))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = USER_GET_BY_ID, notes = USER_PERMIT_ALL_NOTES)
    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable int id) {
        return userService.getById(id);
    }

    @ApiOperation(value = USER_CHANGE_ROLES)
    @PutMapping("/{id}")
    public UserDetails updateRoles(@PathVariable int id, final Authentication authentication) {
        ifUserNotLoggedThrow(authentication);
        return userAuthorityService.changeUserRoles(id);
    }

    @ApiOperation(value = USER_CREATE, notes = USER_PERMIT_ALL_NOTES)
    @PostMapping
    public UserDetails create(@Valid @RequestBody UserCreateDto userCreateDto) {
        UserDetails userDetails = userMapper.dtoToUserCreate(userCreateDto);
        UserDetails newUser = userService.create(userDetails);
        userAuthorityService.createNewUser(userCreateDto);
        return newUser;
    }

    @ApiOperation(value = USER_UPDATE, notes = USER_PRIVATE_NOTES)
    @PutMapping
    public UserDetails update(@Valid @RequestBody UserUpdateDto userUpdateDto, final Authentication authentication) {
        ifUserNotLoggedThrow(authentication);
        UserDetails userToUpdate = userMapper.dtoToUserUpdate(userUpdateDto);
        return userService.update(userToUpdate, authentication.getName());
    }

    @ApiOperation(value = USER_DELETE_BY_ID, notes = USER_PRIVATE_NOTES)
    @DeleteMapping("/{id}")
    public UserDetails delete(@PathVariable int id, final Authentication authentication) {
        ifUserNotLoggedThrow(authentication);
        UserDetails userToDelete = userService.getById(id);
        boolean canDeleteUsers = adminHelper.isRootAdmin(authentication);
        userAuthorityService.deleteUser(userToDelete, authentication.getName(), canDeleteUsers);
        return userService.delete(userToDelete, authentication.getName(), canDeleteUsers);
    }

    private void ifUserNotLoggedThrow(final Authentication authentication) {
        if (authentication == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, EDIT_NOT_AUTHORIZED);
        }
    }
}