package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.exceptions.EmptyPictureException;
import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.exceptions.IOPictureException;
import com.safetycar.safetycar.exceptions.WrongPictureTypeException;
import com.safetycar.safetycar.models.dtos.policy.PolicyCreateDto;
import com.safetycar.safetycar.models.dtos.policy.PolicyInfoDto;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.mappers.contracts.PolicyMapper;
import com.safetycar.safetycar.services.contracts.OfferService;
import com.safetycar.safetycar.services.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

import static com.safetycar.safetycar.utils.enums.PolicyStatus.CANCELED;
import static java.lang.String.format;

@Controller
@ControllerAdvice
@RequestMapping("/policies")
public class PolicyController {

    private final PolicyService policyService;
    private final PolicyMapper policyMapper;
    private final OfferService offerService;
    private final Environment environment;

    @Autowired
    public PolicyController(PolicyService policyService,
                            PolicyMapper policyMapper,
                            OfferService offerService,
                            Environment environment) {
        this.policyService = policyService;
        this.policyMapper = policyMapper;
        this.offerService = offerService;
        this.environment = environment;
    }

    @GetMapping
    public String showPolicyPage(Model model, HttpSession httpSession) {
        model.addAttribute("policyCreateDto", new PolicyCreateDto());
        Offer theOffer = (Offer) httpSession.getAttribute("theOffer");
        return theOffer == null
                ? "redirect:/"
                : "policies/policy-create";
    }

    @PostMapping("/create")
    public String createPolicy(@Valid @ModelAttribute PolicyCreateDto policyCreateDto,
                               BindingResult bindingResult,
                               Model model,
                               String action,
                               HttpSession httpSession,
                               Principal principal) {
        Offer theOffer = (Offer) httpSession.getAttribute("theOffer");
        if (action.equals("cancel")) {
            offerService.deleteById(theOffer.getId());
            httpSession.removeAttribute("theOffer");
            return "redirect:/";
        }

        int policyId = 0;
        if (action.equals("finish")) {
            if (bindingResult.hasErrors()) {
                model.addAttribute("policyCreateDto", policyCreateDto);
                return "policies/policy-create";
            }
            try {
                Policy policy = policyMapper.dtoToCreate(policyCreateDto, principal.getName(), theOffer.getId());
                policyService.create(policy);
                policyId = policy.getId();
                httpSession.removeAttribute("theOffer");
            } catch (EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "policies/policy-create";
            } catch (EmptyPictureException | WrongPictureTypeException | IOPictureException e) {
                model.addAttribute("picError", e.getMessage());
                return "policies/policy-create";
            }
        }
        return format("redirect:/policies/%d/success", policyId);
    }

    @GetMapping("/{policyId}/success")
    public String showPolicySuccessPage(@PathVariable int policyId, Model model) {
        try {
            Policy policy = policyService.getById(policyId);
            PolicyInfoDto policyInfo = policyMapper.policyToInfoDto(policy);
            model.addAttribute("policyInfo", policyInfo);
            return "policies/policy-success";
        } catch (EntityNotFoundException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/cancelPolicy")
    public String cancelPolicy(@RequestParam int id) {
        Policy policyToBeCancelled = policyService.getById(id);
        policyService.processPolicy(policyToBeCancelled, CANCELED);
        return "redirect:/users/profile/policies";
    }

    // global access in this controller
    @ModelAttribute("theOffer")
    public Offer takeOfferFromSession(HttpSession httpSession) {
        return (Offer) httpSession.getAttribute("theOffer");
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handlerFileUploadError(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("picError", format("You could not upload image bigger then %sMB",
                environment.getProperty("spring.servlet.multipart.max-file-size")));
        return "redirect:/policies";
    }
}