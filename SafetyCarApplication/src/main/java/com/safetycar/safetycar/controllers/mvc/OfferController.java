package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.dtos.offer.OfferCreateDto;
import com.safetycar.safetycar.models.dtos.offer.OfferDetailsDto;
import com.safetycar.safetycar.models.entities.Brand;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.models.mappers.contracts.OfferMapper;
import com.safetycar.safetycar.services.contracts.BrandService;
import com.safetycar.safetycar.services.contracts.ModelService;
import com.safetycar.safetycar.services.contracts.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.HashSet;

@Controller
@RequestMapping("/offers")
public class OfferController {

    private final OfferService offerService;
    private final OfferMapper offerMapper;
    private final ModelService modelService;
    private final BrandService brandService;

    @Autowired
    public OfferController(OfferService offerService,
                           OfferMapper offerMapper,
                           ModelService modelService,
                           BrandService brandService) {
        this.offerService = offerService;
        this.offerMapper = offerMapper;
        this.modelService = modelService;
        this.brandService = brandService;
    }

    @GetMapping
    public String showOfferCreatePage(Model model) {
        model.addAttribute("offerCreateDto", new OfferCreateDto());
        return "offers/offer-create";
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute OfferCreateDto offerCreateDto,
                         BindingResult bindingResult,
                         HttpSession httpSession,
                         RedirectAttributes redirectAttributes,
                         Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("offerCreateDto", offerCreateDto);
            return "offers/offer-create";
        }

        try {
            Offer theOffer = offerMapper.dtoToOfferCreate(offerCreateDto);
            // add it to session if user accepts it, to be saved in db
            httpSession.setAttribute("theOffer", theOffer);
            // visualize it in html, only for the next request
            redirectAttributes.addFlashAttribute("offer", theOffer);
            return "redirect:/offers/success";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "offers/offer-create";
        }
    }

    @GetMapping("/success")
    public String offerSuccessPage() {
        return "offers/offer-success";
    }

    @PostMapping("/accept")
    public String acceptOffer(HttpSession httpSession,
                              Principal principal,
                              Model model) {
        // when user is logged and he accepts the offer, save it in database and redirect him to policy page
        Offer theOffer = (Offer) httpSession.getAttribute("theOffer");
        Offer offer = offerService.create(theOffer);
        model.addAttribute("offer", offer);

        return principal == null
                ? "redirect:/login"
                : "redirect:/policies";
    }

    // if user rejects the current calculated offer, remove the offer from the session
    // the offer is saved in the database, only if the user accepts the calculated offer
    @GetMapping("/reject")
    public String rejectOffer(HttpSession httpSession) {
        httpSession.removeAttribute("theOffer");
        return "redirect:/";
    }

    @GetMapping("/offerDetails")
    public String showOfferDetails(@RequestParam int id, Model model) {
        Offer offer = offerService.getById(id);
        OfferDetailsDto offerDetailsDto = offerMapper.offerToDtoDetails(offer);
        model.addAttribute("offerDetailsDto", offerDetailsDto);
        return "offers/offer-details";
    }

    // global access in this controller
    @ModelAttribute("models")
    public Collection<com.safetycar.safetycar.models.entities.Model> allCarModels() {
        return new HashSet<>(modelService.getAll());
    }

    // global access in this controller
    @ModelAttribute("brands")
    public Collection<Brand> allCarBrands() {
        return new HashSet<>(brandService.getAll());
    }
}