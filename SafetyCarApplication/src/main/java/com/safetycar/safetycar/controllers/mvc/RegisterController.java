package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.exceptions.EntityDuplicatedException;
import com.safetycar.safetycar.models.dtos.user.UserCreateDto;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.models.mappers.contracts.UserMapper;
import com.safetycar.safetycar.services.contracts.UserAuthorityService;
import com.safetycar.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegisterController {

    private final UserMapper userMapper;
    private final UserService userService;
    private final UserDetailsManager userDetailsManager;
    private final UserAuthorityService userAuthorityService;

    @Autowired
    public RegisterController(UserMapper userMapper,
                              UserService userService,
                              UserDetailsManager userDetailsManager,
                              UserAuthorityService userAuthorityService) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.userAuthorityService = userAuthorityService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userCreateDto", new UserCreateDto());
        return "users/register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserCreateDto userCreateDto,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userCreateDto", userCreateDto);
            model.addAttribute("error", "Username/password cannot be empty, please try again.");
            return "users/register";
        }
        if (userDetailsManager.userExists(userCreateDto.getUsername())) {
            model.addAttribute("userCreateDto", userCreateDto);
            model.addAttribute("error", "User with the same username already exists, please try again.");
            return "users/register";
        }
        if (!userCreateDto.getPassword().equals(userCreateDto.getConfirmPassword())) {
            model.addAttribute("userCreateDto", userCreateDto);
            model.addAttribute("error", "Passwords do not match, please try again.");
            return "users/register";
        }
        try {
            UserDetails userDetails = userMapper.dtoToUserCreate(userCreateDto);
            userService.create(userDetails);
            userAuthorityService.createNewUser(userCreateDto);
        } catch (EntityDuplicatedException e) {
            model.addAttribute("userCreateDto", userCreateDto);
            model.addAttribute("error", "This email already exists, please try again.");
            return "users/register";
        }
        return "redirect:/login";
    }
}