package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.exceptions.BadRequestException;
import com.safetycar.safetycar.exceptions.EntityDuplicatedException;
import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.exceptions.UnauthorizedRequestException;
import com.safetycar.safetycar.models.rest.RestApiErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.Set;

@ControllerAdvice(basePackages = "com.safetycar.safetycar.controllers.rest")
public class RestApiErrorController extends ResponseEntityExceptionHandler {

    // handles invalid data from the dto, when inserting into the range base amount table
    @ExceptionHandler
    public ResponseEntity<RestApiErrorResponse> baseAmountExceptionHandler(ConstraintViolationException exception) {
        Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
        var builder = new StringBuilder();
        violations.stream()
                .findFirst()
                .ifPresent(violation -> builder.append(violation.getMessage()));

        var errorBody = new RestApiErrorResponse();
        errorBody.setStatus(HttpStatus.BAD_REQUEST.value());
        errorBody.setMessage(builder.toString());
        errorBody.setTimeStamp(LocalDateTime.now());
        return new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<RestApiErrorResponse> sortPoliciesExceptionHandler(BadRequestException exception) {
        var errorBody = new RestApiErrorResponse();
        errorBody.setMessage(exception.getMessage());
        errorBody.setStatus(HttpStatus.BAD_REQUEST.value());
        errorBody.setTimeStamp(LocalDateTime.now());
        return new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<RestApiErrorResponse> entityNotFoundExceptionHandler(EntityNotFoundException exception) {
        var errorBody = new RestApiErrorResponse();
        errorBody.setMessage(exception.getMessage());
        errorBody.setStatus(HttpStatus.NOT_FOUND.value());
        errorBody.setTimeStamp(LocalDateTime.now());
        return new ResponseEntity<>(errorBody, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = EntityDuplicatedException.class)
    public ResponseEntity<RestApiErrorResponse> entityDuplicatedExceptionHandler(EntityDuplicatedException exception) {
        var errorBody = new RestApiErrorResponse();
        errorBody.setMessage(exception.getMessage());
        errorBody.setStatus(HttpStatus.CONFLICT.value());
        errorBody.setTimeStamp(LocalDateTime.now());
        return new ResponseEntity<>(errorBody, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = UnauthorizedRequestException.class)
    public ResponseEntity<RestApiErrorResponse> unauthorizedExceptionHandler(UnauthorizedRequestException exception) {
        var errorBody = new RestApiErrorResponse();
        errorBody.setMessage(exception.getMessage());
        errorBody.setStatus(HttpStatus.UNAUTHORIZED.value());
        errorBody.setTimeStamp(LocalDateTime.now());
        return new ResponseEntity<>(errorBody, HttpStatus.UNAUTHORIZED);
    }
}