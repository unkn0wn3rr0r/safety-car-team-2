package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.dtos.offer.OfferCreateDto;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.models.mappers.contracts.OfferMapper;
import com.safetycar.safetycar.services.contracts.OfferService;
import com.safetycar.safetycar.utils.helpers.contracts.AdminHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.*;

@RestController
@RequestMapping("/api/v1/offers")
public class OfferRestController {

    private static final String DELETE_NOT_AUTHORIZED = "Not authorized to delete unused offers!";

    private final OfferMapper offerMapper;
    private final OfferService offerService;
    private final AdminHelper adminHelper;

    @Autowired
    public OfferRestController(OfferMapper offerMapper,
                               OfferService offerService,
                               AdminHelper adminHelper) {
        this.offerMapper = offerMapper;
        this.offerService = offerService;
        this.adminHelper = adminHelper;
    }

    @ApiOperation(value = GET_ALL_OFFERS)
    @GetMapping
    public List<Offer> getAll() {
        return offerService.getAll();
    }

    @ApiOperation(value = GET_OFFER_BY_ID)
    @GetMapping("/{id}")
    public Offer takeById(@PathVariable int id) {
        return offerService.getById(id);
    }

    @ApiOperation(value = CREATE_OFFER)
    @PostMapping
    public Offer create(@Valid @RequestBody OfferCreateDto offerCreateDto) {
        Offer offer = offerMapper.dtoToOfferCreate(offerCreateDto);
        return offerService.create(offer);
    }

    @ApiOperation(value = DELETE_OFFERS_UNUSED)
    @DeleteMapping("/unused")
    public List<Offer> deleteAllUnusedOffers(final Authentication authentication) {
        if (!adminHelper.isRootAdmin(authentication)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, DELETE_NOT_AUTHORIZED);
        }
        return offerService.deleteAllUnusedOffers();
    }

    @ApiOperation(value = DELETE_OFFER_BY_ID)
    @DeleteMapping("/{id}")
    public Offer deleteById(@PathVariable int id) {
        return offerService.deleteById(id);
    }
}