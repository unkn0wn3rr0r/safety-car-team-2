package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.exceptions.EntityDuplicatedException;
import com.safetycar.safetycar.exceptions.PasswordMismatchException;
import com.safetycar.safetycar.models.dtos.user.UserChangePasswordDto;
import com.safetycar.safetycar.models.dtos.user.UserInfoDto;
import com.safetycar.safetycar.models.dtos.user.UserProfileUpdateDto;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.models.mappers.contracts.UserMapper;
import com.safetycar.safetycar.services.contracts.PolicyService;
import com.safetycar.safetycar.services.contracts.UserAuthorityService;
import com.safetycar.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.safetycar.safetycar.utils.enums.PolicyStatus.*;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserMapper userMapper;
    private final UserService userService;
    private final UserAuthorityService userAuthorityService;
    private final PolicyService policyService;

    @Autowired
    public UserController(UserMapper userMapper,
                          UserService userService,
                          UserAuthorityService userAuthorityService,
                          PolicyService policyService) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.userAuthorityService = userAuthorityService;
        this.policyService = policyService;
    }

    @GetMapping("/profile")
    public String showProfilePage(Model model, Principal principal) {
        UserDetails loggedUser = userService.findByUsername(principal.getName());
        UserInfoDto userInfo = userMapper.userToDtoInfo(loggedUser);
        model.addAttribute("userInfo", userInfo);
        return "users/profile-page";
    }

    @GetMapping("/profile/changePass")
    public String showChangePasswordPage(Model model) {
        model.addAttribute("userChangePasswordDto", new UserChangePasswordDto());
        return "users/change-password";
    }

    @PostMapping("/updatePass")
    public String updateUserPassword(@Valid @ModelAttribute UserChangePasswordDto userChangePasswordDto,
                                     BindingResult bindingResult,
                                     Principal principal,
                                     Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userChangePasswordDto", userChangePasswordDto);
            return "users/change-password";
        }
        try {
            userAuthorityService.changeUserPassword(userChangePasswordDto, principal.getName());
        } catch (PasswordMismatchException e) {
            bindingResult.rejectValue("confirmNewPassword", "userChangePasswordDto", e.getMessage());
            return "users/change-password";
        }
        return "redirect:/users/profile";
    }

    @GetMapping("/profile/info")
    public String showUpdateInfoPage(Model model, Principal principal) {
        UserDetails userByUsername = userService.findByUsername(principal.getName());
        UserProfileUpdateDto userProfileUpdateDto = userMapper.userToUpdateInfoDto(userByUsername);
        model.addAttribute("userProfileUpdateDto", userProfileUpdateDto);
        return "users/update-info";
    }

    @PostMapping("/update")
    public String updateInfo(@Valid @ModelAttribute UserProfileUpdateDto userProfileUpdateDto,
                             BindingResult bindingResult,
                             Authentication authentication,
                             Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userProfileUpdateDto", userProfileUpdateDto);
            return "users/update-info";
        }
        try {
            UserDetails userToUpdateInfo = userMapper.dtoToUserUpdateInfo(userProfileUpdateDto);
            userService.update(userToUpdateInfo, authentication.getName());
        } catch (EntityDuplicatedException e) {
            bindingResult.rejectValue("email", "userProfileUpdateDto", e.getMessage());
            return "users/update-info";
        }
        return "redirect:/users/profile";
    }

    @GetMapping("/profile/policies")
    public String showPoliciesPage(Model model, Principal principal) {
        UserDetails userByUsername = userService.findByUsername(principal.getName());
        int userId = userByUsername.getId();

        List<Policy> userPolicies = policyService.getAllUserPoliciesById(userId);
        long policiesCount = policyService.countAllUserPoliciesById(userId);
        long approvedPolicies = policyService.countUserPoliciesByIdAndStatus(userId, APPROVED.name().toLowerCase());
        long pendingPolicies = policyService.countUserPoliciesByIdAndStatus(userId, PENDING.name().toLowerCase());
        long cancelledPolicies = policyService.countUserPoliciesByIdAndStatus(userId, CANCELED.name().toLowerCase());

        model.addAttribute("userPolicies", userPolicies);
        model.addAttribute("policiesCount", policiesCount);
        model.addAttribute("approvedPolicies", approvedPolicies);
        model.addAttribute("pendingPolicies", pendingPolicies);
        model.addAttribute("cancelledPolicies", cancelledPolicies);
        return "users/policies-page";
    }
}