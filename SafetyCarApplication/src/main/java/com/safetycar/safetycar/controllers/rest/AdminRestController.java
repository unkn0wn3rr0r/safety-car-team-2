package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.services.contracts.PolicyService;
import com.safetycar.safetycar.services.contracts.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.*;

@RestController
@RequestMapping("/api/v1/admins")
public class AdminRestController {

    private final PolicyService policyService;
    private final UserService userService;

    @Autowired
    public AdminRestController(PolicyService policyService,
                               UserService userService) {
        this.policyService = policyService;
        this.userService = userService;
    }

    @ApiOperation(value = GET_ALL_ADMINS)
    @GetMapping
    public List<Authority> getAllAdmins() {
        return userService.getAllAdmins();
    }

    @ApiOperation(value = ADMIN_SORT_POLICIES, notes = ADMIN_SORT_EXTRA_INFO)
    @GetMapping("/sort")
    public List<Policy> sort(@RequestParam String sort) {
        return policyService.sort(sort);
    }
}