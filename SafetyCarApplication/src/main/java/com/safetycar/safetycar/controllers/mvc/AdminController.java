package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.services.contracts.NotificationService;
import com.safetycar.safetycar.services.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.safetycar.safetycar.utils.enums.PolicyStatus.*;
import static java.lang.String.format;

@Controller
@RequestMapping("/admins")
public class AdminController {

    private final PolicyService policyService;
    private final NotificationService notificationService;

    @Autowired
    public AdminController(PolicyService policyService,
                           NotificationService notificationService) {
        this.policyService = policyService;
        this.notificationService = notificationService;
    }

    @GetMapping
    public String showAdminPage(Model model) {
        model.addAttribute("approvedStatuses", policyService.getPoliciesSizeByStatusCode(APPROVED.ordinal()));
        model.addAttribute("pendingStatuses", policyService.getPendingPolicies().size());
        model.addAttribute("canceledStatuses", policyService.getPoliciesSizeByStatusCode(CANCELED.ordinal()));
        model.addAttribute("rejectedStatuses", policyService.getPoliciesSizeByStatusCode(REJECTED.ordinal()));
        return "admins/admin-page";
    }

    @GetMapping("/pending")
    public String showPendingPoliciesPage(Model model) {
        model.addAttribute("pendingPolicies", policyService.getPendingPolicies());
        return "admins/pending-policies-page";
    }

    @PostMapping("/reject")
    public String rejectPolicy(@RequestParam int id) {
        try {
            Policy policy = policyService.getById(id);
            UserDetails user = policy.getUserDetails();
            policyService.processPolicy(policy, REJECTED);
            notificationService.sendPolicyRejectedNotification(user, policy.getEmail());
        } catch (EntityNotFoundException e) {
            return "redirect:/";
        } catch (MailException e) {
            return "exceptions/mail-failed";
        }
        return format("redirect:/admins/policy/%d/info", id);
    }

    @PostMapping("/approve")
    public String approvePolicy(@RequestParam int id) {
        try {
            Policy policy = policyService.getById(id);
            UserDetails user = policy.getUserDetails();
            policyService.processPolicy(policy, APPROVED);
            notificationService.sendPolicyApprovedNotification(user, policy.getEmail());
        } catch (EntityNotFoundException e) {
            return "redirect:/";
        } catch (MailException e) {
            return "exceptions/mail-failed";
        }
        return format("redirect:/admins/policy/%d/info", id);
    }

    @GetMapping("/all")
    public String showAllPoliciesPage(Model model) {
        model.addAttribute("policies", policyService.getAll());
        return "admins/all-policies-page";
    }

    @GetMapping("/users")
    public String showAllUsers() {
        return "admins/all-users-page";
    }

    @GetMapping("/sort")
    public String sort(@RequestParam String sort, Model model) {
        model.addAttribute("policies", policyService.sort(sort));
        return "admins/all-policies-page";
    }

    @GetMapping("/policy/{id}/info")
    public String showPolicyInfo(@PathVariable int id, Model model) {
        try {
            Policy policy = policyService.getById(id);
            model.addAttribute("user", policy.getUserDetails());
            model.addAttribute("offer", policy.getOffer());
            model.addAttribute("policy", policy);
        } catch (EntityNotFoundException e) {
            return "redirect:/";
        }
        return "admins/policy-review";
    }
}