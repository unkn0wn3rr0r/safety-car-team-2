package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.dtos.car.BrandAjaxDto;
import com.safetycar.safetycar.models.dtos.car.ModelAjaxDto;
import com.safetycar.safetycar.models.mappers.contracts.CarMapper;
import com.safetycar.safetycar.services.contracts.BrandService;
import com.safetycar.safetycar.services.contracts.ModelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.CARS_BRANDS;
import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.CARS_MODELS;

@RestController
@RequestMapping("/api/v1")
public class CarRestController {

    private final ModelService modelService;
    private final BrandService brandService;
    private final CarMapper carMapper;

    @Autowired
    public CarRestController(ModelService modelService,
                             BrandService brandService,
                             CarMapper carMapper) {
        this.modelService = modelService;
        this.brandService = brandService;
        this.carMapper = carMapper;
    }

    @ApiOperation(value = CARS_MODELS)
    @GetMapping("/models/{id}")
    public List<ModelAjaxDto> generateAllBrandModels(@PathVariable int id) {
        return modelService
                .getAll()
                .stream()
                .filter(model -> model.getBrand().getId() == id)
                .map(carMapper::carModelToDto)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = CARS_BRANDS)
    @GetMapping("/brands")
    public List<BrandAjaxDto> getAll() {
        return brandService
                .getAll()
                .stream()
                .map(carMapper::carBrandToDto)
                .collect(Collectors.toList());
    }
}