package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.dtos.policy.PolicyUserListDto;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.mappers.contracts.PolicyMapper;
import com.safetycar.safetycar.services.contracts.PolicyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.*;

@RestController
@RequestMapping("/api/v1/policies")
public class PolicyRestController {

    private final PolicyService policyService;
    private final PolicyMapper policyMapper;

    @Autowired
    public PolicyRestController(PolicyService policyService,
                                PolicyMapper policyMapper) {
        this.policyService = policyService;
        this.policyMapper = policyMapper;
    }

    @ApiOperation(value = POLICIES_GET_ALL)
    @GetMapping
    public List<Policy> getAll() {
        return policyService.getAll();
    }

    @ApiOperation(value = POLICIES_GET_ALL_BY_ID)
    @GetMapping("/{id}/user")
    public List<PolicyUserListDto> getAllById(@PathVariable int id) {
        return policyService
                .getAllUserPoliciesById(id)
                .stream()
                .map(policyMapper::policyToUserPolicyListDto)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = GET_POLICY_BY_ID)
    @GetMapping("/{id}")
    public Policy getById(@PathVariable int id) {
        return policyService.getById(id);
    }
}