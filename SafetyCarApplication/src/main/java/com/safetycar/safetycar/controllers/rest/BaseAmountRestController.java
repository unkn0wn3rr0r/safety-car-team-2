package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.dtos.baseamount.BaseAmountUploadTableDataDto;
import com.safetycar.safetycar.models.entities.BaseAmount;
import com.safetycar.safetycar.models.mappers.contracts.BaseAmountMapper;
import com.safetycar.safetycar.services.contracts.BaseAmountService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.safetycar.safetycar.utils.constants.SwaggerDocumentationMessages.*;

/**
 * This class is made with the assumption that only the ADMIN/ROOT has the authorization to manage data in the range tables.
 */

@Validated
@RestController
@RequestMapping("/api/v1/base-amount")
public class BaseAmountRestController {

    private final BaseAmountService baseAmountService;
    private final BaseAmountMapper baseAmountMapper;

    @Autowired
    public BaseAmountRestController(BaseAmountService baseAmountService,
                                    BaseAmountMapper baseAmountMapper) {
        this.baseAmountService = baseAmountService;
        this.baseAmountMapper = baseAmountMapper;
    }

    @ApiOperation(value = BASE_AMOUNT_GET_ALL, notes = BASE_AMOUNT_EXTRA_INFO)
    @GetMapping
    public List<BaseAmount> getAll() {
        return baseAmountService.getAll();
    }

    @ApiOperation(value = BASE_AMOUNT_INSERT_DATA, notes = BASE_AMOUNT_EXTRA_INFO)
    @PostMapping
    public List<BaseAmount> insertTableData(@Valid @RequestBody List<BaseAmountUploadTableDataDto> baseAmountUploadTableDataDto) {
        List<BaseAmount> baseAmountDataList = baseAmountUploadTableDataDto
                .stream()
                .map(baseAmountMapper::dtoToBaseAmount)
                .collect(Collectors.toList());

        return baseAmountService.insertTableData(baseAmountDataList);
    }
}