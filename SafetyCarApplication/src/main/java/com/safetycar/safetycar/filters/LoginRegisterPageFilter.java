package com.safetycar.safetycar.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginRegisterPageFilter extends GenericFilter {

    private final Logger logger = LoggerFactory.getLogger(LoginRegisterPageFilter.class);

    // filter which redirects authenticated users back to home page, when trying to access login or register page
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String requestURI = ((HttpServletRequest) request).getRequestURI();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated() &&
                ("/login".equals(requestURI) || "/register".equals(requestURI))) {
            logger.info("===> User: {} is authenticated, but trying to access login/register page, redirecting to /", authentication.getName());
            ((HttpServletResponse) response).sendRedirect("/");
        }

        chain.doFilter(request, response);
    }
}