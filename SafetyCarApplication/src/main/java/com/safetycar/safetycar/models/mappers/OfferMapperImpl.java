package com.safetycar.safetycar.models.mappers;

import com.safetycar.safetycar.models.dtos.offer.OfferCreateDto;
import com.safetycar.safetycar.models.dtos.offer.OfferDetailsDto;
import com.safetycar.safetycar.models.entities.Brand;
import com.safetycar.safetycar.models.entities.Model;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.models.mappers.contracts.OfferMapper;
import com.safetycar.safetycar.services.contracts.BrandService;
import com.safetycar.safetycar.services.contracts.ModelService;
import com.safetycar.safetycar.utils.helpers.contracts.OfferCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OfferMapperImpl implements OfferMapper {

    private final OfferCalculator offerCalculator;
    private final ModelService modelService;
    private final BrandService brandService;

    @Autowired
    public OfferMapperImpl(OfferCalculator offerCalculator,
                           ModelService modelService,
                           BrandService brandService) {
        this.offerCalculator = offerCalculator;
        this.modelService = modelService;
        this.brandService = brandService;
    }

    @Override
    public Offer dtoToOfferCreate(OfferCreateDto offerCreateDto) {
        var offer = new Offer();
        offer.setCubicCapacity(offerCreateDto.getCubicCapacity());
        offer.setRegistrationDate(offerCreateDto.getRegistrationDate());
        offer.setDriverAge(offerCreateDto.getDriverAge());
        offer.setHasAccidents(offerCreateDto.isHasAccidents());

        double baseAmount = offerCalculator.calculateBaseAmount(offer);
        offer.setTotalPremium(baseAmount);

        Model model = modelService.getById(offerCreateDto.getModelId());
        Brand brand = brandService.getById(offerCreateDto.getBrandId());
        model.setBrand(brand);
        offer.setModel(model);
        return offer;
    }

    @Override
    public OfferDetailsDto offerToDtoDetails(Offer offer) {
        var offerDetailsDto = new OfferDetailsDto();
        offerDetailsDto.setCubicCapacity(offer.getCubicCapacity());
        offerDetailsDto.setRegistrationDate(offer.getRegistrationDate());
        offerDetailsDto.setDriverAge(offer.getDriverAge());

        boolean hasAccidents = offer.isHasAccidents();
        offerDetailsDto.setHasAccidents(hasAccidents ? "Yes" : "No");
        offerDetailsDto.setModel(offer.getModel().getName());
        offerDetailsDto.setBrand(offer.getModel().getBrand().getName());
        offerDetailsDto.setTotalPremium(offer.getTotalPremium());
        return offerDetailsDto;
    }
}