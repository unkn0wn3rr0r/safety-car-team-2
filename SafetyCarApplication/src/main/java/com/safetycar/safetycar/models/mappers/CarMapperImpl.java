package com.safetycar.safetycar.models.mappers;

import com.safetycar.safetycar.models.dtos.car.BrandAjaxDto;
import com.safetycar.safetycar.models.dtos.car.ModelAjaxDto;
import com.safetycar.safetycar.models.entities.Brand;
import com.safetycar.safetycar.models.entities.Model;
import com.safetycar.safetycar.models.mappers.contracts.CarMapper;
import org.springframework.stereotype.Component;

@Component
public class CarMapperImpl implements CarMapper {

    @Override
    public BrandAjaxDto carBrandToDto(Brand brand) {
        var brandAjaxDto = new BrandAjaxDto();
        brandAjaxDto.setId(brand.getId());
        brandAjaxDto.setName(brand.getName());
        return brandAjaxDto;
    }

    @Override
    public ModelAjaxDto carModelToDto(Model model) {
        var modelAjaxDto = new ModelAjaxDto();
        modelAjaxDto.setId(model.getId());
        modelAjaxDto.setName(model.getName());
        return modelAjaxDto;
    }
}