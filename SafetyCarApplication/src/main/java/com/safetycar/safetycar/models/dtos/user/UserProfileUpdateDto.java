package com.safetycar.safetycar.models.dtos.user;

import javax.validation.constraints.*;

public class UserProfileUpdateDto {

    private int id;

    @NotEmpty(message = "Username cannot be empty!")
    @Size(min = 3, max = 15, message = "Username must be between 3 and 15 characters!")
    private String username;

    @Email(message = "Invalid email!")
    @NotEmpty(message = "Email cannot be empty!")
    @Size(min = 5, max = 30, message = "Email must be between 5 and 30 characters!")
    private String email;

    @NotEmpty(message = "First name cannot be empty!")
    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;

    @NotEmpty(message = "Last name cannot be empty!")
    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;

    @NotNull(message = "Age cannot be empty!")
    @Min(value = 18, message = "Cannot be negative or under 18 years old!")
    private Integer age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}