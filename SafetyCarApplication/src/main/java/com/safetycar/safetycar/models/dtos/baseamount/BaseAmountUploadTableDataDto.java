package com.safetycar.safetycar.models.dtos.baseamount;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class BaseAmountUploadTableDataDto {

    @NotNull(message = "Cubic capacity min cannot be empty!")
    @Min(value = 0, message = "Cubic capacity min should be zero or greater!")
    @Max(value = 999999, message = "Cubic capacity min should not be greater than 999999!")
    private int cubicCapacityMin;

    @NotNull(message = "Cubic capacity max cannot be empty!")
    @Min(value = 1047, message = "Cubic capacity max should be 1047 or greater!")
    @Max(value = 999999, message = "Cubic capacity max should not be greater than 999999!")
    private int cubicCapacityMax;

    @NotNull(message = "Car age min cannot be empty!")
    @Min(value = 0, message = "Car age min should be zero or greater!")
    @Max(value = 999, message = "Car age min should not be greater than 999!")
    private int carAgeMin;

    @NotNull(message = "Car age max cannot be empty!")
    @Min(value = 19, message = "Car age max should be 19 or greater!")
    @Max(value = 999, message = "Car age max should not be greater than 999!")
    private int carAgeMax;

    @NotNull(message = "Base amount cannot be empty!")
    @Min(value = 100, message = "Base amount should be 100 or greater!")
    private double baseAmount;

    public int getCubicCapacityMin() {
        return cubicCapacityMin;
    }

    public void setCubicCapacityMin(int cubicCapacityMin) {
        this.cubicCapacityMin = cubicCapacityMin;
    }

    public int getCubicCapacityMax() {
        return cubicCapacityMax;
    }

    public void setCubicCapacityMax(int cubicCapacityMax) {
        this.cubicCapacityMax = cubicCapacityMax;
    }

    public int getCarAgeMin() {
        return carAgeMin;
    }

    public void setCarAgeMin(int carAgeMin) {
        this.carAgeMin = carAgeMin;
    }

    public int getCarAgeMax() {
        return carAgeMax;
    }

    public void setCarAgeMax(int carAgeMax) {
        this.carAgeMax = carAgeMax;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }
}