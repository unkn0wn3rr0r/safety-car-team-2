package com.safetycar.safetycar.models.mappers.contracts;

import com.safetycar.safetycar.models.dtos.offer.OfferCreateDto;
import com.safetycar.safetycar.models.dtos.offer.OfferDetailsDto;
import com.safetycar.safetycar.models.entities.Offer;

public interface OfferMapper {

    Offer dtoToOfferCreate(OfferCreateDto offerCreateDto);

    OfferDetailsDto offerToDtoDetails(Offer offer);
}