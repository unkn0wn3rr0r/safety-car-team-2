package com.safetycar.safetycar.models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "base_amount")
public class BaseAmount extends BaseEntity {

    @Column(name = "cc_min")
    private int cubicCapacityMin;

    @Column(name = "cc_max")
    private int cubicCapacityMax;

    @Column(name = "car_age_min")
    private int carAgeMin;

    @Column(name = "car_age_max")
    private int carAgeMax;

    @Column(name = "base_amount")
    private Double baseAmount;

    public BaseAmount() {
    }

    public int getCubicCapacityMin() {
        return cubicCapacityMin;
    }

    public void setCubicCapacityMin(int cubicCapacityMin) {
        this.cubicCapacityMin = cubicCapacityMin;
    }

    public int getCubicCapacityMax() {
        return cubicCapacityMax;
    }

    public void setCubicCapacityMax(int cubicCapacityMax) {
        this.cubicCapacityMax = cubicCapacityMax;
    }

    public int getCarAgeMin() {
        return carAgeMin;
    }

    public void setCarAgeMin(int carAgeMin) {
        this.carAgeMin = carAgeMin;
    }

    public int getCarAgeMax() {
        return carAgeMax;
    }

    public void setCarAgeMax(int carAgeMax) {
        this.carAgeMax = carAgeMax;
    }

    public Double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Double baseAmount) {
        this.baseAmount = baseAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseAmount)) return false;
        BaseAmount that = (BaseAmount) o;
        return cubicCapacityMin == that.cubicCapacityMin &&
                cubicCapacityMax == that.cubicCapacityMax &&
                carAgeMin == that.carAgeMin &&
                carAgeMax == that.carAgeMax &&
                Objects.equals(baseAmount, that.baseAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cubicCapacityMin, cubicCapacityMax, carAgeMin, carAgeMax, baseAmount);
    }
}