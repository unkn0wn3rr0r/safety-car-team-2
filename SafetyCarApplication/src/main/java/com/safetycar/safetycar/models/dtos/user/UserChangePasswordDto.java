package com.safetycar.safetycar.models.dtos.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserChangePasswordDto {

    @NotEmpty(message = "Invalid password, must not be empty")
    @Size(min = 5, max = 20, message = "Password must be between 5 and 20 characters!")
    private String oldPassword;

    @NotEmpty(message = "Invalid password, must not be empty")
    @Size(min = 5, max = 20, message = "Password must be between 5 and 20 characters!")
    private String newPassword;

    @NotEmpty(message = "Invalid password, must not be empty")
    @Size(min = 5, max = 20, message = "Password must be between 5 and 20 characters!")
    private String confirmNewPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
}