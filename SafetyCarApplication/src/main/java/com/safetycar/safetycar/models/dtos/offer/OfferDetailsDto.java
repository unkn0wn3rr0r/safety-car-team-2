package com.safetycar.safetycar.models.dtos.offer;

import java.time.LocalDate;

public class OfferDetailsDto {

    private int cubicCapacity;
    private LocalDate registrationDate;
    private int driverAge;
    private String hasAccidents;
    private String model;
    private String brand;
    private double totalPremium;

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public String getHasAccidents() {
        return hasAccidents;
    }

    public void setHasAccidents(String hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }
}