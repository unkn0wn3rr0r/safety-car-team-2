package com.safetycar.safetycar.models.rest;

import java.time.LocalDateTime;

public class RestApiErrorResponse {

    private int status;
    private String message;
    private LocalDateTime timeStamp;

    public RestApiErrorResponse() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
}