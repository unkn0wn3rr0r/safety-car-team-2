package com.safetycar.safetycar.models.dtos.policy;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class PolicyCreateDto {

    @NotEmpty(message = "Postal address cannot be empty")
    private String postalAddress;

    @Email(message = "Invalid email!")
    @NotEmpty(message = "Email cannot be empty!")
    private String email;

    @NotEmpty(message = "Phone cannot be empty!")
    @Pattern(regexp = "(^$|[0-9]{10})", message = "Please enter your phone number")
    private String phone;

    private MultipartFile picture;

    public PolicyCreateDto() {
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public MultipartFile getPicture() {
        return picture;
    }

    public void setPicture(MultipartFile file) {
        this.picture = file;
    }
}