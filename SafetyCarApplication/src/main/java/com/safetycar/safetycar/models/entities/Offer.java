package com.safetycar.safetycar.models.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "offers")
public class Offer extends BaseEntity {

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "registration_date")
    private LocalDate registrationDate;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "has_accidents")
    private boolean hasAccidents;

    @Column(name = "total_premium")
    private double totalPremium;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Model model;

    public Offer() {
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isHasAccidents() {
        return hasAccidents;
    }

    public void setHasAccidents(boolean hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    public double getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "cubicCapacity=" + cubicCapacity +
                ", registrationDate=" + registrationDate +
                ", driverAge=" + driverAge +
                ", hasAccidents=" + hasAccidents +
                ", totalPremium=" + totalPremium +
                ", model=" + model +
                '}';
    }
}