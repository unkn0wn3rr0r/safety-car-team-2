package com.safetycar.safetycar.models.mappers;

import com.safetycar.safetycar.models.dtos.user.*;
import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.models.mappers.contracts.UserMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDetails dtoToUserCreate(UserCreateDto userCreateDto) {
        var userToCreate = new UserDetails();
        userToCreate.setUsername(userCreateDto.getUsername());
        userToCreate.setEmail(userCreateDto.getEmail());
        userToCreate.setFirstName(userCreateDto.getFirstName());
        userToCreate.setLastName(userCreateDto.getLastName());
        userToCreate.setAge(userCreateDto.getAge());
        userToCreate.setEnabled(true);
        return userToCreate;
    }

    @Override
    public UserDetails dtoToUserUpdate(UserUpdateDto userUpdateDto) {
        var userToUpdate = new UserDetails();
        userToUpdate.setId(userUpdateDto.getUserId());
        userToUpdate.setUsername(userUpdateDto.getUsername());
        userToUpdate.setEmail(userUpdateDto.getEmail());
        userToUpdate.setFirstName(userUpdateDto.getFirstName());
        userToUpdate.setLastName(userUpdateDto.getLastName());
        userToUpdate.setAge(userUpdateDto.getAge());
        userToUpdate.setEnabled(true);
        return userToUpdate;
    }

    @Override
    public UserProfileUpdateDto userToUpdateInfoDto(UserDetails user) {
        var userProfileUpdateDto = new UserProfileUpdateDto();
        userProfileUpdateDto.setId(user.getId());
        userProfileUpdateDto.setUsername(user.getUsername());
        userProfileUpdateDto.setEmail(user.getEmail());
        userProfileUpdateDto.setFirstName(user.getFirstName());
        userProfileUpdateDto.setLastName(user.getLastName());
        userProfileUpdateDto.setAge(user.getAge());
        return userProfileUpdateDto;
    }

    @Override
    public UserDetails dtoToUserUpdateInfo(UserProfileUpdateDto userProfileUpdateDto) {
        var userToUpdateInfo = new UserDetails();
        userToUpdateInfo.setId(userProfileUpdateDto.getId());
        userToUpdateInfo.setUsername(userProfileUpdateDto.getUsername());
        userToUpdateInfo.setEmail(userProfileUpdateDto.getEmail());
        userToUpdateInfo.setFirstName(userProfileUpdateDto.getFirstName());
        userToUpdateInfo.setLastName(userProfileUpdateDto.getLastName());
        userToUpdateInfo.setAge(userProfileUpdateDto.getAge());
        return userToUpdateInfo;
    }


    @Override
    public UserInfoDto userToDtoInfo(UserDetails user) {
        var userInfoDto = new UserInfoDto();
        userInfoDto.setUsername(user.getUsername());
        String fullName = user.getFirstName() + " " + user.getLastName();
        userInfoDto.setFullName(fullName);
        userInfoDto.setEmail(user.getEmail());
        return userInfoDto;
    }

    @Override
    public UserDto userDetailsAndAdminsToUserDto(List<Authority> admins, UserDetails user) {
        var userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setFullName(user.getFirstName() + " " + user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setAge(user.getAge());

        admins.stream()
                .filter(admin -> admin.getUsername().equals(user.getUsername()))
                .map(admin -> "Admin")
                .forEach(userDto::setRole);

        return userDto;
    }
}