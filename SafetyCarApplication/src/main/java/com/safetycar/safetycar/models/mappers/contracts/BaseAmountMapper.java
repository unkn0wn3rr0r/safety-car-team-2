package com.safetycar.safetycar.models.mappers.contracts;

import com.safetycar.safetycar.models.dtos.baseamount.BaseAmountUploadTableDataDto;
import com.safetycar.safetycar.models.entities.BaseAmount;

public interface BaseAmountMapper {

    BaseAmount dtoToBaseAmount(BaseAmountUploadTableDataDto baseAmountUploadTableDataDto);
}