package com.safetycar.safetycar.models.mappers;

import com.safetycar.safetycar.exceptions.EmptyPictureException;
import com.safetycar.safetycar.exceptions.IOPictureException;
import com.safetycar.safetycar.exceptions.WrongPictureTypeException;
import com.safetycar.safetycar.models.dtos.policy.PolicyCreateDto;
import com.safetycar.safetycar.models.dtos.policy.PolicyInfoDto;
import com.safetycar.safetycar.models.dtos.policy.PolicyUserListDto;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.entities.Status;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.models.mappers.contracts.PolicyMapper;
import com.safetycar.safetycar.services.contracts.OfferService;
import com.safetycar.safetycar.services.contracts.StatusService;
import com.safetycar.safetycar.services.contracts.UserService;
import com.safetycar.safetycar.utils.enums.PolicyStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;

@Component
public class PolicyMapperImpl implements PolicyMapper {

    private static final String PICTURE_NOT_SELECTED = "Not selected picture!";
    private static final String PICTURE_TYPE = "Only .png and .jpg allowed!";
    private static final String PICTURE_IO = "Something is wrong with your picture, please try to upload it again!";

    private final UserService userService;
    private final OfferService offerService;
    private final StatusService statusService;

    @Autowired
    public PolicyMapperImpl(UserService userService,
                            OfferService offerService,
                            StatusService statusService) {
        this.userService = userService;
        this.offerService = offerService;
        this.statusService = statusService;
    }

    @Override
    public Policy dtoToCreate(PolicyCreateDto policyCreateDto,
                              String username,
                              Integer offerId) {
        UserDetails userId = userService.findByUsername(username);
        Offer offer = offerService.getById(offerId);
        Status status = statusService.getById(PolicyStatus.PENDING.getCODE());

        var policy = new Policy();
        policy.setEffectiveDate(LocalDate.now());
        policy.setPostalAddress(policyCreateDto.getPostalAddress());
        policy.setEmail(policyCreateDto.getEmail());
        policy.setPhone(policyCreateDto.getPhone());
        policy.setStatus(status);
        policy.setOffer(offer);
        policy.setUserDetails(userId);

        try {
            if (policyCreateDto.getPicture().isEmpty()) {
                throw new EmptyPictureException(PICTURE_NOT_SELECTED);
            }
            if (!("image/jpeg".equals(policyCreateDto.getPicture().getContentType()) || ("image/png").equals(policyCreateDto.getPicture().getContentType()))) {
                throw new WrongPictureTypeException(PICTURE_TYPE);
            }
            policy.setRegistrationCertificateImage(policyCreateDto.getPicture().getBytes());
        } catch (IOException e) {
            throw new IOPictureException(PICTURE_IO);
        }
        return policy;
    }

    @Override
    public PolicyInfoDto policyToInfoDto(Policy policy) {
        var policyInfoDto = new PolicyInfoDto();
        policyInfoDto.setDate(policy.getEffectiveDate().toString());
        policyInfoDto.setAddress(policy.getPostalAddress());
        policyInfoDto.setEmail(policy.getEmail());
        String fullName = policy.getUserDetails().getFirstName() + " " + policy.getUserDetails().getLastName();
        policyInfoDto.setName(fullName);
        return policyInfoDto;
    }

    @Override
    public PolicyUserListDto policyToUserPolicyListDto(Policy policy) {
        var policyUserListDto = new PolicyUserListDto();
        policyUserListDto.setId(policy.getId());
        policyUserListDto.setEffectiveDate(policy.getEffectiveDate());
        policyUserListDto.setPhone(policy.getPhone());
        policyUserListDto.setCubicCapacity(policy.getOffer().getCubicCapacity());
        String carModel = policy.getOffer().getModel().getBrand().getName() + " " + policy.getOffer().getModel().getName();
        policyUserListDto.setCarModel(carModel);
        policyUserListDto.setDriverAge(policy.getOffer().getDriverAge());
        policyUserListDto.setHasAccidents(policy.getOffer().isHasAccidents() ? "Yes" : "No");
        policyUserListDto.setStatusName(policy.getStatus().getName());
        policyUserListDto.setPostalAddress(policy.getPostalAddress());
        policyUserListDto.setTotalPremium(policy.getOffer().getTotalPremium());
        return policyUserListDto;
    }
}