package com.safetycar.safetycar.models.mappers.contracts;

import com.safetycar.safetycar.models.dtos.car.BrandAjaxDto;
import com.safetycar.safetycar.models.dtos.car.ModelAjaxDto;
import com.safetycar.safetycar.models.entities.Brand;
import com.safetycar.safetycar.models.entities.Model;

public interface CarMapper {

    BrandAjaxDto carBrandToDto(Brand brand);

    ModelAjaxDto carModelToDto(Model model);
}