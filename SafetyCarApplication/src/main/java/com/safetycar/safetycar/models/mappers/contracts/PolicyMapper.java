package com.safetycar.safetycar.models.mappers.contracts;

import com.safetycar.safetycar.models.dtos.policy.PolicyCreateDto;
import com.safetycar.safetycar.models.dtos.policy.PolicyInfoDto;
import com.safetycar.safetycar.models.dtos.policy.PolicyUserListDto;
import com.safetycar.safetycar.models.entities.Policy;

public interface PolicyMapper {

    Policy dtoToCreate(PolicyCreateDto policyCreateDto, String username, Integer offerId);

    PolicyInfoDto policyToInfoDto(Policy policy);

    PolicyUserListDto policyToUserPolicyListDto(Policy policy);
}