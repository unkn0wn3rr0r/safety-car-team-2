package com.safetycar.safetycar.models.mappers;

import com.safetycar.safetycar.models.dtos.baseamount.BaseAmountUploadTableDataDto;
import com.safetycar.safetycar.models.entities.BaseAmount;
import com.safetycar.safetycar.models.mappers.contracts.BaseAmountMapper;
import org.springframework.stereotype.Component;

@Component
public class BaseAmountMapperImpl implements BaseAmountMapper {

    @Override
    public BaseAmount dtoToBaseAmount(BaseAmountUploadTableDataDto baseAmountUploadTableDataDto) {
        var baseAmountData = new BaseAmount();
        baseAmountData.setCubicCapacityMin(baseAmountUploadTableDataDto.getCubicCapacityMin());
        baseAmountData.setCubicCapacityMax(baseAmountUploadTableDataDto.getCubicCapacityMax());
        baseAmountData.setCarAgeMin(baseAmountUploadTableDataDto.getCarAgeMin());
        baseAmountData.setCarAgeMax(baseAmountUploadTableDataDto.getCarAgeMax());
        baseAmountData.setBaseAmount(baseAmountUploadTableDataDto.getBaseAmount());
        return baseAmountData;
    }
}