package com.safetycar.safetycar.models.dtos.offer;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class OfferCreateDto {

    @NotNull(message = "Cubic capacity cannot be empty")
    @Range(min = 1, max = 999999, message = "Cubic capacity must be between 1 and 999999")
    private Integer cubicCapacity;

    @NotNull(message = "Registration date cannot be empty")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate registrationDate;

    @NotNull(message = "Driver age cannot be empty")
    @Range(min = 18, max = 999, message = "Driver age must be between 18 and 999 years")
    private Integer driverAge;

    @NotNull(message = "Has accidents cannot be empty")
    private boolean hasAccidents;

    @NotNull(message = "Please select car model")
    private Integer modelId;

    @NotNull(message = "Please select car brand")
    private Integer brandId;

    public OfferCreateDto() {
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(Integer driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isHasAccidents() {
        return hasAccidents;
    }

    public void setHasAccidents(boolean hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }
}