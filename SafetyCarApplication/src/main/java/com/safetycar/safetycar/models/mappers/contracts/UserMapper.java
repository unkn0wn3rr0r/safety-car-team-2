package com.safetycar.safetycar.models.mappers.contracts;

import com.safetycar.safetycar.models.dtos.user.*;
import com.safetycar.safetycar.models.entities.Authority;
import com.safetycar.safetycar.models.entities.UserDetails;

import java.util.List;

public interface UserMapper {

    UserDetails dtoToUserCreate(UserCreateDto userCreateDto);

    UserDetails dtoToUserUpdate(UserUpdateDto userUpdateDto);

    UserProfileUpdateDto userToUpdateInfoDto(UserDetails user);

    UserDetails dtoToUserUpdateInfo(UserProfileUpdateDto userProfileUpdateDto);

    UserInfoDto userToDtoInfo(UserDetails user);

    UserDto userDetailsAndAdminsToUserDto(List<Authority> admins, UserDetails user);
}