package com.safetycar.safetycar.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "policies")
public class Policy extends BaseEntity {

    @Column(name = "effective_date")
    private LocalDate effectiveDate;

    @JsonIgnore
    @Column(name = "registration_certificate_image")
    private byte[] registrationCertificateImage;

    @Column(name = "postal_address")
    private String postalAddress;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "offer_id")
    private Offer offer;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetails userDetails;

    public Policy() {

    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public byte[] getRegistrationCertificateImage() {
        return registrationCertificateImage;
    }

    public void setRegistrationCertificateImage(byte[] registrationCertificateImage) {
        this.registrationCertificateImage = registrationCertificateImage;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public String generateBase64Image() {
        return Base64.encodeBase64String(getRegistrationCertificateImage());
    }
}