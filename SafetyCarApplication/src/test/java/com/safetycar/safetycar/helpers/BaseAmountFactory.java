package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.entities.BaseAmount;

public class BaseAmountFactory {

    public static BaseAmount createMockBaseAmount(int id) {
        var baseAmount = new BaseAmount();
        baseAmount.setId(id);
        baseAmount.setCarAgeMin(0);
        baseAmount.setCarAgeMax(19);
        baseAmount.setCubicCapacityMin(0);
        baseAmount.setCubicCapacityMax(1047);
        baseAmount.setBaseAmount(403.25);
        return baseAmount;
    }

    public static BaseAmount createMockBaseAmount2(int id) {
        var baseAmount = new BaseAmount();
        baseAmount.setId(id);
        baseAmount.setCarAgeMin(20);
        baseAmount.setCarAgeMax(999);
        baseAmount.setCubicCapacityMin(1048);
        baseAmount.setCubicCapacityMax(1309);
        baseAmount.setBaseAmount(413.25);
        return baseAmount;
    }
}