package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.entities.Brand;

public class BrandFactory {

    public static Brand createMockBrand() {
        var brand = new Brand();
        brand.setId(1);
        brand.setName("Audi");
        return brand;
    }
}