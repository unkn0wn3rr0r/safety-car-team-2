package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.entities.Policy;

import java.time.LocalDate;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;
import static com.safetycar.safetycar.helpers.OfferFactory.createMockOffer;
import static com.safetycar.safetycar.helpers.StatusFactory.*;
import static com.safetycar.safetycar.helpers.UserDetailsFactory.createMockUser;

public class PolicyFactory {

    private static final String HEX_STRING = "e04fd020ea3a6910a2d808002b30309d";
    private static final String POSTAL_ADDRESS = "1234";
    private static final String EMAIL = "admin1234@gmail.com";
    private static final String PHONE_NUMBER = "1234567890";

    public static Policy createMockPendingPolicy(int policyId) {
        var offer = createMockOffer();
        var user = createMockUser();
        var status = setPendingMockStatus();
        var localDate = LocalDate.now();
        var picture = hexStringToByteArray(HEX_STRING);
        var policy = new Policy();
        policy.setId(policyId);
        policy.setEffectiveDate(localDate);
        policy.setPostalAddress(POSTAL_ADDRESS);
        policy.setEmail(EMAIL);
        policy.setPhone(PHONE_NUMBER);
        policy.setStatus(status);
        policy.setOffer(offer);
        policy.setUserDetails(user);
        policy.setRegistrationCertificateImage(picture);
        return policy;
    }

    public static Policy createMockCanceledPolicy(int policyId) {
        var offer = createMockOffer();
        var user = createMockUser();
        var status = setCanceledMockStatus();
        var localDate = LocalDate.now();
        var picture = hexStringToByteArray(HEX_STRING);
        var policy = new Policy();
        policy.setId(policyId);
        policy.setEffectiveDate(localDate);
        policy.setPostalAddress(POSTAL_ADDRESS);
        policy.setEmail(EMAIL);
        policy.setPhone(PHONE_NUMBER);
        policy.setStatus(status);
        policy.setOffer(offer);
        policy.setUserDetails(user);
        policy.setRegistrationCertificateImage(picture);
        return policy;
    }

    public static Policy createMockApprovedPolicy(int policyId) {
        var offer = createMockOffer();
        var user = createMockUser();
        var status = setApprovedMockStatus();
        var localDate = LocalDate.now();
        var picture = hexStringToByteArray(HEX_STRING);
        var policy = new Policy();
        policy.setId(policyId);
        policy.setEffectiveDate(localDate);
        policy.setPostalAddress(POSTAL_ADDRESS);
        policy.setEmail(EMAIL);
        policy.setPhone(PHONE_NUMBER);
        policy.setStatus(status);
        policy.setOffer(offer);
        policy.setUserDetails(user);
        policy.setRegistrationCertificateImage(picture);
        return policy;
    }

    public static Policy createMockRejectedPolicy(int policyId) {
        var offer = createMockOffer();
        var user = createMockUser();
        var status = setRejectedMockStatus();
        var localDate = LocalDate.now();
        var picture = hexStringToByteArray(HEX_STRING);
        var policy = new Policy();
        policy.setId(policyId);
        policy.setEffectiveDate(localDate);
        policy.setPostalAddress(POSTAL_ADDRESS);
        policy.setEmail(EMAIL);
        policy.setPhone(PHONE_NUMBER);
        policy.setStatus(status);
        policy.setOffer(offer);
        policy.setUserDetails(user);
        policy.setRegistrationCertificateImage(picture);
        return policy;
    }
}