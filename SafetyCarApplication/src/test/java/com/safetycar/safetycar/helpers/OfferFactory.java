package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.entities.Offer;

import java.time.LocalDate;

import static com.safetycar.safetycar.helpers.ModelFactory.createMockModel;

public class OfferFactory {

    public static Offer createMockOffer() {
        var localDate = LocalDate.now();
        var model = createMockModel();
        var offer = new Offer();
        offer.setRegistrationDate(localDate);
        offer.setTotalPremium(900);
        offer.setHasAccidents(true);
        offer.setCubicCapacity(2000);
        offer.setDriverAge(21);
        offer.setModel(model);
        offer.setId(1);
        return offer;
    }
}