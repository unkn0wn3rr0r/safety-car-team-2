package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.entities.Status;

public class StatusFactory {

    private static final int[] STATUS_CODES = {1, 2, 3, 4};
    private static final String[] STATUS_NAMES = {"rejected", "approved", "canceled", "pending"};

    public static Status setPendingMockStatus() {
        var status = new Status();
        status.setId(STATUS_CODES[3]);
        status.setName(STATUS_NAMES[3]);
        return status;
    }

    public static Status setCanceledMockStatus() {
        var status = new Status();
        status.setId(STATUS_CODES[2]);
        status.setName(STATUS_NAMES[2]);
        return status;
    }

    public static Status setApprovedMockStatus() {
        var status = new Status();
        status.setId(STATUS_CODES[1]);
        status.setName(STATUS_NAMES[1]);
        return status;
    }

    public static Status setRejectedMockStatus() {
        var status = new Status();
        status.setId(STATUS_CODES[0]);
        status.setName(STATUS_NAMES[0]);
        return status;
    }
}