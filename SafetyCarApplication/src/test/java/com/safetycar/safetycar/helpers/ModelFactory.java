package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.entities.Model;

import static com.safetycar.safetycar.helpers.BrandFactory.createMockBrand;

public class ModelFactory {

    public static Model createMockModel() {
        var model = new Model();
        model.setId(1);
        model.setName("A4");
        model.setBrand(createMockBrand());
        return model;
    }
}