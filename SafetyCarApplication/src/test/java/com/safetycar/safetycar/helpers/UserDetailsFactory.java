package com.safetycar.safetycar.helpers;

import com.safetycar.safetycar.models.dtos.user.UserChangePasswordDto;
import com.safetycar.safetycar.models.entities.UserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import java.util.List;

import static com.safetycar.safetycar.utils.enums.UserRoleType.*;

public class UserDetailsFactory {

    public static final int USER_VALID_ID_TEST = 1;
    public static final int USER_INVALID_ID_TEST = 2;
    public static final String USER_EMAIL_TEST = "john_doe@gmail.com";
    public static final String USER_USERNAME_TEST = "john_doe";
    public static final String USER_FIRST_NAME_TEST = "John";
    public static final String USER_LAST_NAME_TEST = "Doe";
    public static final String USER_PASSWORD = "12341234";
    public static final int USER_AGE_TEST = 25;
    public static final boolean USER_IS_ENABLED = true;

    public static UserDetails createMockUser() {
        var user = new UserDetails();
        user.setId(USER_VALID_ID_TEST);
        user.setUsername(USER_USERNAME_TEST);
        user.setFirstName(USER_FIRST_NAME_TEST);
        user.setLastName(USER_LAST_NAME_TEST);
        user.setEmail(USER_EMAIL_TEST);
        user.setAge(USER_AGE_TEST);
        user.setEnabled(USER_IS_ENABLED);
        return user;
    }

    public static UserDetails createUserWithId() {
        var user = new UserDetails();
        user.setId(USER_VALID_ID_TEST);
        user.setEmail(USER_EMAIL_TEST);
        user.setUsername(USER_USERNAME_TEST);
        user.setFirstName(USER_FIRST_NAME_TEST);
        user.setLastName(USER_LAST_NAME_TEST);
        user.setAge(USER_AGE_TEST);
        user.setEnabled(USER_IS_ENABLED);
        return user;
    }

    public static UserDetails createUserWithoutId() {
        var user = new UserDetails();
        user.setEmail(USER_EMAIL_TEST);
        user.setUsername(USER_USERNAME_TEST);
        user.setFirstName(USER_FIRST_NAME_TEST);
        user.setLastName(USER_LAST_NAME_TEST);
        user.setAge(USER_AGE_TEST);
        user.setEnabled(USER_IS_ENABLED);
        return user;
    }

    public static User createSpringSecurityUser() {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(
                ROLE_USER.name(),
                ROLE_ADMIN.name(),
                ROLE_ROOT.name());

        return new User(
                USER_USERNAME_TEST,
                USER_PASSWORD,
                false,
                true,
                true,
                true,
                authorities);
    }

    public static UserChangePasswordDto createUserChangePasswordDto() {
        var changeDto = new UserChangePasswordDto();
        changeDto.setOldPassword(USER_PASSWORD);
        changeDto.setNewPassword(USER_PASSWORD);
        changeDto.setConfirmNewPassword(USER_PASSWORD);
        return changeDto;
    }
}