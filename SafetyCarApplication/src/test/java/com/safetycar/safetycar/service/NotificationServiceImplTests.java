package com.safetycar.safetycar.service;

import com.safetycar.safetycar.services.NotificationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;

import static com.safetycar.safetycar.helpers.UserDetailsFactory.createMockUser;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceImplTests {

    @InjectMocks
    NotificationServiceImpl mockNotificationService;

    @Mock
    JavaMailSender mockJavaMailSender;

    @Mock
    Environment mockEnvironment;

    @Test
    public void sendMessage_shouldSend_policyApproved_notificationMessage() {
        // Arrange
        var mockedUser = createMockUser();
        var mockedNotificationService = mock(NotificationServiceImpl.class);

        // Act Assert
        when(mockEnvironment.getProperty("spring.mail.username"))
                .thenReturn("something");

        lenient()
                .doNothing()
                .when(mockedNotificationService)
                .sendPolicyApprovedNotification(mockedUser, mockedUser.getEmail());

        mockNotificationService.sendPolicyApprovedNotification(mockedUser, mockedUser.getEmail());
    }

    @Test
    public void sendMessage_shouldSend_policyRejected_notificationMessage() {
        // Arrange
        var mockedUser = createMockUser();
        var mockedNotificationService = mock(NotificationServiceImpl.class);

        // Act Assert
        when(mockEnvironment.getProperty("spring.mail.username"))
                .thenReturn("something");

        lenient()
                .doNothing()
                .when(mockedNotificationService)
                .sendPolicyRejectedNotification(mockedUser, mockedUser.getEmail());

        mockNotificationService.sendPolicyRejectedNotification(mockedUser, mockedUser.getEmail());
    }
}