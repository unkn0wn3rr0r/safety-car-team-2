package com.safetycar.safetycar.service;

import com.safetycar.safetycar.models.entities.BaseAmount;
import com.safetycar.safetycar.repositories.contracts.BaseAmountRepository;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.BaseAmountServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.safetycar.safetycar.helpers.BaseAmountFactory.createMockBaseAmount;
import static com.safetycar.safetycar.helpers.BaseAmountFactory.createMockBaseAmount2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BaseAmountServiceImplTests {

    @InjectMocks
    BaseAmountServiceImpl baseAmountService;

    @Mock
    BaseAmountRepository mockBaseAmountRepository;

    @Mock
    GenericRepositoryHelper<BaseAmount> baseAmountGenericRepositoryHelper;

    @Test
    public void getAll_ShouldReturnEmptyList() {
        //Arrange
        when(baseAmountGenericRepositoryHelper.getAll(BaseAmount.class))
                .thenReturn(new ArrayList<>());

        //Act
        List<BaseAmount> resultBaseAmounts = baseAmountService.getAll();

        //Assert
        verify(baseAmountGenericRepositoryHelper, times(1))
                .getAll(BaseAmount.class);

        assertEquals(0, resultBaseAmounts.size());
    }

    @Test
    public void search_ShouldReturn() {
        //Arrange
        int age = 10;
        int cubicCapacity = 1000;
        BaseAmount baseAmount = createMockBaseAmount(1);
        when(mockBaseAmountRepository.search(cubicCapacity, age))
                .thenReturn(baseAmount);

        //Act
        BaseAmount resultBaseAmount = baseAmountService.search(cubicCapacity, age);

        // Assert
        verify(mockBaseAmountRepository, times(1))
                .search(cubicCapacity, age);

        assertEquals(baseAmount, resultBaseAmount);
    }

    @Test
    public void insertTableDate_ShouldInsertData_WhenTableIsEmpty() {
        //Arrange
        List<BaseAmount> baseAmounts = new ArrayList<>();
        baseAmounts.add(createMockBaseAmount(1));
        doNothing().when(mockBaseAmountRepository).insertTableData(baseAmounts);

        //Act
        baseAmountService.insertTableData(baseAmounts);

        //Assert
        verify(mockBaseAmountRepository, times(1))
                .insertTableData(baseAmounts);
    }

    @Test
    public void insertTableDate_ShouldFilterAndInsertData_WhenTableIsNotEmpty() {
        //Arrange
        List<BaseAmount> baseAmounts = new ArrayList<>();
        List<BaseAmount> baseAmounts2 = new ArrayList<>();
        baseAmounts.add(createMockBaseAmount(1));
        baseAmounts.add(createMockBaseAmount2(2));

        baseAmounts2.add(createMockBaseAmount(1));

        when(baseAmountGenericRepositoryHelper.getAll(BaseAmount.class))
                .thenReturn(baseAmounts2);

        ////Act
        List<BaseAmount> resultBaseAmount = baseAmountService.insertTableData(baseAmounts);

        //Assert
        assertEquals(1, resultBaseAmount.size());
    }
}