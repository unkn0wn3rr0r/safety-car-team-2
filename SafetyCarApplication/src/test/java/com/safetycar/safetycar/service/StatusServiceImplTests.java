package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Status;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.StatusServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.safetycar.safetycar.helpers.StatusFactory.setCanceledMockStatus;
import static com.safetycar.safetycar.helpers.StatusFactory.setPendingMockStatus;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {

    @InjectMocks
    StatusServiceImpl statusService;

    @Mock
    GenericRepositoryHelper<Status> mockStatusRepository;

    @Test
    public void getAdd_ShouldReturnList() {
        //Arrange
        List<Status> statuses = new ArrayList<>();
        statuses.add(setPendingMockStatus());
        statuses.add(setCanceledMockStatus());
        when(mockStatusRepository.getAll(Status.class))
                .thenReturn(statuses);
        //Act
        List<Status> resultStatuses = statusService.getAll();

        //Assert
        verify(mockStatusRepository, times(1))
                .getAll(Status.class);

        assertEquals(2, resultStatuses.size());
    }

    @Test
    public void getAdd_ShouldReturnAllEmptyList_WhenThereIsNotStatuses() {
        //Arrange
        when(mockStatusRepository.getAll(Status.class))
                .thenReturn(new ArrayList<>());
        //Act
        List<Status> resultStatuses = statusService.getAll();

        //Assert
        verify(mockStatusRepository, times(1))
                .getAll(Status.class);

        assertEquals(0, resultStatuses.size());
    }

    @Test
    public void getById_ShouldThrow_WhenThereIsNotStatusWithThisId() {
        //Arrange
        when(mockStatusRepository.getById(eq(Status.class), anyInt()))
                .thenThrow(EntityNotFoundException.class);

        //Act, Assert
        verify(mockStatusRepository, times(0))
                .getById(eq(Status.class), anyInt());

        assertThrows(EntityNotFoundException.class,
                () -> statusService.getById(0));
    }

    @Test
    public void getById_ShouldReturnStatus_WhenThereIsStatusWithThisId() {
        //Arrange
        Status status = setPendingMockStatus();
        when(mockStatusRepository.getById(Status.class, 4))
                .thenReturn(status);

        //Act
        Status returnStatus = statusService.getById(4);

        //Assert
        verify(mockStatusRepository, times(1))
                .getById(Status.class, 4);

        assertEquals(status, returnStatus);
    }
}