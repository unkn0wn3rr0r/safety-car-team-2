package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Brand;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.BrandServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.safetycar.safetycar.helpers.BrandFactory.createMockBrand;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BrandServiceImplTests {

    @InjectMocks
    BrandServiceImpl brandService;

    @Mock
    GenericRepositoryHelper<Brand> mockBrandRepository;

    @Test
    public void getAll_shouldReturn_emptyCollection() {
        // Arrange
        when(mockBrandRepository.getAll(Brand.class))
                .thenReturn(new ArrayList<>());

        // Act
        var allBrands = brandService.getAll();

        // Assert
        assertEquals(0, allBrands.size());
        assertTrue(allBrands.isEmpty());

        verify(mockBrandRepository, times(1)).getAll(Brand.class);
    }

    @Test
    public void getAll_shouldReturn_notEmptyCollection() {
        // Arrange
        when(mockBrandRepository.getAll(Brand.class))
                .thenReturn(of(createMockBrand()));

        // Act
        var allBrands = brandService.getAll();

        // Assert
        assertEquals(1, allBrands.size());
        assertFalse(allBrands.isEmpty());

        verify(mockBrandRepository, times(1)).getAll(Brand.class);
    }

    @Test
    public void getById_shouldThrow_whenDoesNotExists() {
        // Arrange
        when(mockBrandRepository.getById(Brand.class, 1))
                .thenThrow(EntityNotFoundException.class);

        // Act Assert
        assertThrows(EntityNotFoundException.class,
                () -> brandService.getById(1));

        verify(mockBrandRepository, times(1)).getById(Brand.class, 1);
    }

    @Test
    public void getById_shouldReturnBrand_whenExists() {
        // Arrange
        var theBrand = createMockBrand();

        when(mockBrandRepository.getById(Brand.class, 1))
                .thenReturn(theBrand);

        // Act
        var brand = brandService.getById(1);

        // Assert
        assertNotNull(brand);
        assertEquals(theBrand, brand);

        verify(mockBrandRepository, times(1)).getById(Brand.class, 1);
    }
}