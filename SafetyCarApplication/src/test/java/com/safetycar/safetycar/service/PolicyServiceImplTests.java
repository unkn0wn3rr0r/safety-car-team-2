package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.BadRequestException;
import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Policy;
import com.safetycar.safetycar.models.entities.Status;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.repositories.contracts.PolicyRepository;
import com.safetycar.safetycar.services.PolicyServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.safetycar.safetycar.helpers.PolicyFactory.*;
import static com.safetycar.safetycar.helpers.StatusFactory.*;
import static com.safetycar.safetycar.helpers.UserDetailsFactory.createMockUser;
import static com.safetycar.safetycar.utils.enums.PolicyStatus.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PolicyServiceImplTests {

    @InjectMocks
    PolicyServiceImpl policyService;

    @Mock
    PolicyRepository mockPolicyRepository;

    @Mock
    GenericRepositoryHelper<Policy> policyGenericRepositoryHelper;

    @Test
    public void getAll_ShouldReturnAllList() {
        //Arrange
        List<Policy> policies = new ArrayList<>();
        policies.add(createMockPendingPolicy(1));
        policies.add(createMockPendingPolicy(2));
        policies.add(createMockPendingPolicy(3));
        when(policyGenericRepositoryHelper.getAll(Policy.class))
                .thenReturn(policies);

        //Act
        List<Policy> resultPolicies = policyService.getAll();

        //Assert
        verify(policyGenericRepositoryHelper, times(1))
                .getAll(Policy.class);

        assertEquals(policies.size(), resultPolicies.size());
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenWhereIsNotPolicies() {
        //Arrange
        when(policyGenericRepositoryHelper.getAll(Policy.class))
                .thenReturn(new ArrayList<>());

        //Act
        List<Policy> resultPolicies = policyService.getAll();

        //Assert
        verify(policyGenericRepositoryHelper, times(1))
                .getAll(Policy.class);

        assertEquals(0, resultPolicies.size());
    }

    @Test
    public void getById_ShouldReturnPolicy_WhenThereIsPolicyWithThisId() {
        //Arrange
        Policy originalPolicy = createMockPendingPolicy(1);
        when(policyGenericRepositoryHelper.getById(Policy.class, 1))
                .thenReturn(originalPolicy);

        //Act
        Policy resultPolicy = policyService.getById(1);

        //Assert
        verify(policyGenericRepositoryHelper, times(1))
                .getById(Policy.class, 1);

        assertEquals(originalPolicy, resultPolicy);
    }

    @Test
    public void getById_ShouldThrow_WhenThereIsNotPolicyWithThisId() {
        //Arrange
        when(policyGenericRepositoryHelper.getById(eq(Policy.class), anyInt()))
                .thenThrow(EntityNotFoundException.class);

        //Act,Assert

        verify(policyGenericRepositoryHelper, times(0))
                .getById(eq(Policy.class), anyInt());

        assertThrows(EntityNotFoundException.class,
                () -> policyService.getById(0));
    }

    @Test
    public void create_ShouldReturnPolicy_WhenPolicyIsCreated() {
        //Arrange
        Policy createPolicy = createMockPendingPolicy(1);
        when(mockPolicyRepository.create(createPolicy))
                .thenReturn(createPolicy);

        //Act
        Policy resultPolicy = policyService.create(createPolicy);

        //Assert
        verify(mockPolicyRepository, times(1))
                .create(createPolicy);

        assertEquals(createPolicy, resultPolicy);
    }

    @Test
    public void getAllUserPoliciesById_ShouldReturnPolicies_WhenThereIsPoliciesWithUserId() {
        //Arrange
        UserDetails user = createMockUser();
        List<Policy> userPolicies = new ArrayList<>();
        userPolicies.add(createMockPendingPolicy(1));
        userPolicies.add(createMockPendingPolicy(2));
        userPolicies.add(createMockPendingPolicy(3));
        when(mockPolicyRepository.getAllPoliciesForUserById(user.getId()))
                .thenReturn(userPolicies);

        //Act
        List<Policy> resultUserPolicies = policyService.getAllUserPoliciesById(user.getId());

        //Assert
        verify(mockPolicyRepository, times(1))
                .getAllPoliciesForUserById(user.getId());

        assertEquals(userPolicies, resultUserPolicies);
    }

    @Test
    public void getAllUserPoliciesById_ShouldReturnEmptyList_WhenThereIsNotPoliciesWithUserId() {
        //Arrange
        UserDetails user = createMockUser();
        when(mockPolicyRepository.getAllPoliciesForUserById(user.getId()))
                .thenReturn(new ArrayList<>());

        //Act
        List<Policy> resultUserPolicies = policyService.getAllUserPoliciesById(user.getId());

        //Assert
        verify(mockPolicyRepository, times(1))
                .getAllPoliciesForUserById(user.getId());

        assertEquals(0, resultUserPolicies.size());
    }

    @Test
    public void countAllUserPoliciesById_ShouldReturnCount_WhenThereIsPoliciesWithUserId() {
        //Arrange
        UserDetails user = createMockUser();
        List<Policy> userPolicies = new ArrayList<>();
        userPolicies.add(createMockPendingPolicy(1));
        userPolicies.add(createMockPendingPolicy(2));
        userPolicies.add(createMockPendingPolicy(3));
        Long count = (long) userPolicies.size();
        when(mockPolicyRepository.countAllUserPoliciesById(user.getId()))
                .thenReturn(count);

        //Act
        Long resultCount = policyService.countAllUserPoliciesById(user.getId());

        //Assert
        verify(mockPolicyRepository, times(1))
                .countAllUserPoliciesById(user.getId());

        assertEquals(0, resultCount.compareTo(count));
    }

    @Test
    public void countAllUserPoliciesById_ShouldReturnZero_WhenThereIsNotPoliciesWithUserId() {
        //Arrange
        UserDetails user = createMockUser();
        Long count = (long) 0;
        when(mockPolicyRepository.countAllUserPoliciesById(user.getId()))
                .thenReturn(count);

        //Act
        Long resultCount = policyService.countAllUserPoliciesById(user.getId());

        //Assert
        verify(mockPolicyRepository, times(1))
                .countAllUserPoliciesById(user.getId());

        assertEquals(0, count.compareTo(resultCount));
    }

    @Test
    public void countUserPoliciesByIdAndStatus_ShouldReturnCount_WhenThereIsPoliciesWithUserIdAndStatus() {
        //Arrange
        UserDetails user = createMockUser();
        Status canceledStatus = setCanceledMockStatus();
        Long count = (long) 1;
        mockPolicyRepository.create(createMockCanceledPolicy(1));
        mockPolicyRepository.create(createMockPendingPolicy(2));
        when(mockPolicyRepository.countUserPoliciesByIdAndStatus(user.getId(), canceledStatus.getName()))
                .thenReturn(count);

        //Act
        Long resultCount = policyService.countUserPoliciesByIdAndStatus(user.getId(), canceledStatus.getName());

        //Assert
        verify(mockPolicyRepository, times(1))
                .countUserPoliciesByIdAndStatus(user.getId(), canceledStatus.getName());

        assertEquals(0, count.compareTo(resultCount));
    }

    @Test
    public void countUserPoliciesByIdAndStatus_ShouldReturnCount_WhenThereIsNotPoliciesWithUserIdAndStatus() {
        //Arrange
        UserDetails user = createMockUser();
        Status status = setCanceledMockStatus();
        Long count = (long) 0;
        when(mockPolicyRepository.countUserPoliciesByIdAndStatus(anyInt(), anyString()))
                .thenReturn(count);

        //Act
        Long resultCount = policyService.countUserPoliciesByIdAndStatus(user.getId(), status.getName());

        //Assert
        verify(mockPolicyRepository, times(1))
                .countUserPoliciesByIdAndStatus(user.getId(), status.getName());

        assertEquals(0, count.compareTo(resultCount));
    }

    @Test
    public void cancelPolicy_ShouldReturnPolicy_WhenPolicyIsCanceled() {
        //Arrange
        Policy pendingPolicy = createMockCanceledPolicy(1);
        Policy canceledPolicy = createMockPendingPolicy(1);
        canceledPolicy.setStatus(setCanceledMockStatus());
        when(mockPolicyRepository.update(pendingPolicy))
                .thenReturn(canceledPolicy);

        //Act
        Policy resultPolicy = policyService.processPolicy(pendingPolicy, CANCELED);

        //Assert
        verify(mockPolicyRepository, times(1))
                .update(pendingPolicy);

        assertEquals(canceledPolicy, resultPolicy);
    }

    @Test
    public void approvedPolicy_ShouldReturnPolicy_WhenPolicyIsApproved() {
        //Arrange
        Policy pendingPolicy = createMockPendingPolicy(1);
        Policy approvedPolicy = createMockApprovedPolicy(1);
        approvedPolicy.setStatus(setApprovedMockStatus());
        when(mockPolicyRepository.update(pendingPolicy))
                .thenReturn(approvedPolicy);

        //Act
        Policy resultPolicy = policyService.processPolicy(pendingPolicy, APPROVED);

        //Assert
        verify(mockPolicyRepository, times(1))
                .update(pendingPolicy);

        assertEquals(approvedPolicy, resultPolicy);
    }

    @Test
    public void rejectPolicy_ShouldReturnPolicy_WhenPolicyIsRejected() {
        //Arrange
        Policy pendingPolicy = createMockPendingPolicy(1);
        Policy rejectedPolicy = createMockRejectedPolicy(1);
        rejectedPolicy.setStatus(setRejectedMockStatus());
        when(mockPolicyRepository.update(pendingPolicy))
                .thenReturn(rejectedPolicy);

        //Act
        Policy resultPolicy = policyService.processPolicy(pendingPolicy, REJECTED);

        //Assert
        verify(mockPolicyRepository, times(1))
                .update(pendingPolicy);

        assertEquals(rejectedPolicy, resultPolicy);
    }

    @Test
    public void sortPolicies_ShouldReturn_sortedPolicies_byGivenCriteria() {
        //Arrange
        Policy pendingPolicy = createMockPendingPolicy(1);
        Policy rejectedPolicy = createMockRejectedPolicy(1);

        List<Policy> policiesSorted = List.of(rejectedPolicy, pendingPolicy);

        when(mockPolicyRepository.sort("pending"))
                .thenReturn(policiesSorted);

        //Act
        var policiesSortedFromService = policyService.sort("pending");

        //Assert
        verify(mockPolicyRepository, times(1))
                .sort("pending");

        assertEquals(policiesSorted, policiesSortedFromService);
    }

    @Test
    public void sortPolicies_shouldThrow_whenInvalid_sortCriteria() {
        //Arrange
        when(mockPolicyRepository.sort("invalid"))
                .thenThrow(BadRequestException.class);

        //Act Assert
        verify(mockPolicyRepository, times(0))
                .sort("invalid");

        assertThrows(BadRequestException.class,
                () -> policyService.sort("invalid"));
    }

    @Test
    public void getPendingPolicies_shouldReturn_onlyPendingPolicies() {
        //Arrange
        Policy pendingPolicy = createMockPendingPolicy(1);

        when(mockPolicyRepository.getPendingPolicies())
                .thenReturn(List.of(pendingPolicy));

        //Act
        var pendingPolicies = policyService.getPendingPolicies();

        //Assert
        verify(mockPolicyRepository, times(1))
                .getPendingPolicies();

        assertNotNull(pendingPolicies);
        assertFalse(pendingPolicies.isEmpty());
        assertEquals(pendingPolicies.get(0), pendingPolicy);
    }

    @Test
    public void getPolicies_byStatusCode_shouldReturn_onlyThoseBy_theStatusCode() {
        //Arrange
        when(mockPolicyRepository.getPoliciesSizeByStatusCode(PENDING.ordinal()))
                .thenReturn(1L);

        //Act
        var pendingPoliciesSize = policyService.getPoliciesSizeByStatusCode(PENDING.ordinal());

        //Assert
        verify(mockPolicyRepository, times(1))
                .getPoliciesSizeByStatusCode(PENDING.ordinal());

        assertNotNull(pendingPoliciesSize);
        assertEquals(1, pendingPoliciesSize);
    }
}