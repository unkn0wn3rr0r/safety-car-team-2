package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Model;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.services.ModelServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.safetycar.safetycar.helpers.ModelFactory.createMockModel;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {

    @InjectMocks
    ModelServiceImpl modelService;

    @Mock
    GenericRepositoryHelper<Model> mockModelRepository;

    @Test
    public void getAll_shouldReturn_emptyCollection() {
        // Arrange
        when(mockModelRepository.getAll(Model.class))
                .thenReturn(new ArrayList<>());

        // Act
        var allModels = modelService.getAll();

        // Assert
        assertEquals(0, allModels.size());
        assertTrue(allModels.isEmpty());

        verify(mockModelRepository, times(1)).getAll(Model.class);
    }

    @Test
    public void getAll_shouldReturn_notEmptyCollection() {
        // Arrange
        when(mockModelRepository.getAll(Model.class))
                .thenReturn(of(createMockModel()));

        // Act
        var allModels = modelService.getAll();

        // Assert
        assertEquals(1, allModels.size());
        assertFalse(allModels.isEmpty());

        verify(mockModelRepository, times(1)).getAll(Model.class);
    }

    @Test
    public void getById_shouldThrow_whenNotFound() {
        // Arrange
        when(mockModelRepository.getById(Model.class, 1))
                .thenThrow(EntityNotFoundException.class);

        // Act Assert
        assertThrows(EntityNotFoundException.class,
                () -> modelService.getById(1));

        verify(mockModelRepository, times(1)).getById(Model.class, 1);
    }

    @Test
    public void getById_shouldReturnModel_whenFound() {
        // Arrange
        var theModel = createMockModel();

        when(mockModelRepository.getById(Model.class, 1))
                .thenReturn(theModel);

        // Act
        var model = modelService.getById(1);

        // Assert
        assertNotNull(model);
        assertEquals(theModel, model);

        verify(mockModelRepository, times(1)).getById(Model.class, 1);
    }
}