package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.entities.Offer;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.repositories.contracts.OfferRepository;
import com.safetycar.safetycar.services.OfferServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.safetycar.helpers.OfferFactory.createMockOffer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OfferServiceImplTests {

    @InjectMocks
    OfferServiceImpl offerService;

    @Mock
    OfferRepository mockOfferRepository;

    @Mock
    GenericRepositoryHelper<Offer> offerGenericRepositoryHelper;

    @Test
    public void create_ShouldReturnList_WhenOfferIsCreated() {
        //Arrange
        Offer createOffer = createMockOffer();
        when(mockOfferRepository.create(createOffer))
                .thenReturn(createOffer);

        //Act
        Offer returnOffer = offerService.create(createOffer);

        //Assert
        verify(mockOfferRepository, times(1)).create(createOffer);

        assertEquals(createOffer, returnOffer);
    }

    @Test
    public void getById_ShouldReturnOffer_WhenThereIsOfferWithThisId() {
        //Arrange
        Offer originalOffer = createMockOffer();
        when(offerGenericRepositoryHelper.getById(Offer.class, 1))
                .thenReturn(originalOffer);

        //Act
        Offer returnOffer = offerService.getById(1);

        //Assert
        verify(offerGenericRepositoryHelper, times(1))
                .getById(Offer.class, 1);

        assertEquals(returnOffer, originalOffer);
    }

    @Test
    public void getById_ShouldThrow_WhenThereIsNotOfferWithThisId() {
        //Arrange
        when(offerGenericRepositoryHelper.getById(eq(Offer.class), anyInt()))
                .thenThrow(EntityNotFoundException.class);

        //Act, Assert
        verify(offerGenericRepositoryHelper, times(0))
                .getById(eq(Offer.class), anyInt());

        assertThrows(EntityNotFoundException.class,
                () -> offerService.getById(0));
    }

    @Test
    public void deleteById_ShouldReturnOffer_WhenOfferIsDeleted() {
        //Arrange
        Offer deleteOffer = createMockOffer();
        when(mockOfferRepository.deleteById(1)).thenReturn(deleteOffer);

        //Act
        Offer returnOffer = offerService.deleteById(1);

        //Assert
        verify(mockOfferRepository, times(1))
                .deleteById(anyInt());

        assertEquals(returnOffer, deleteOffer);
    }

    @Test
    public void deleteById_ShouldThrow_WhenThereIsNotOfferWithThisId() {
        //Arrange
        when(mockOfferRepository.deleteById(anyInt()))
                .thenThrow(EntityNotFoundException.class);

        //Act, Assert
        verify(mockOfferRepository, times(0))
                .deleteById(anyInt());

        assertThrows(EntityNotFoundException.class,
                () -> offerService.deleteById(anyInt()));
    }
}