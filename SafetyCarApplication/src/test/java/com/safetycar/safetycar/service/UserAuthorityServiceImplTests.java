package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.PasswordMismatchException;
import com.safetycar.safetycar.models.dtos.user.UserChangePasswordDto;
import com.safetycar.safetycar.services.UserAuthorityServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import static com.safetycar.safetycar.helpers.UserDetailsFactory.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserAuthorityServiceImplTests {

    @InjectMocks
    UserAuthorityServiceImpl mockUserAuthorityService;

    @Mock
    PasswordEncoder mockPasswordEncoder;

    @Mock
    UserDetailsManager mockUserDetailsManager;

    @Test
    public void changePassword_shouldThrow_whenThereIsNotUserWithName() {
        //Arrange
        UserChangePasswordDto changeDto = createUserChangePasswordDto();

        //Act
        when(mockUserDetailsManager.loadUserByUsername(USER_USERNAME_TEST))
                .thenThrow(UsernameNotFoundException.class);

        //Assert
        assertThrows(UsernameNotFoundException.class,
                () -> mockUserAuthorityService.changeUserPassword(changeDto, USER_USERNAME_TEST));
    }

    @Test
    public void changePassword_shouldThrow_whenOldPassword_isInvalid() {
        //Arrange
        UserChangePasswordDto changeDto = createUserChangePasswordDto();
        var userDetails = createSpringSecurityUser();

        when(mockUserDetailsManager.loadUserByUsername(USER_USERNAME_TEST))
                .thenReturn(userDetails);

        when(mockPasswordEncoder.matches(changeDto.getOldPassword(), userDetails.getPassword()))
                .thenReturn(false);

        //Act, Assert
        assertThrows(PasswordMismatchException.class,
                () -> mockUserAuthorityService.changeUserPassword(changeDto, USER_USERNAME_TEST));
    }

    @Test
    public void changePassword_shouldChangePassword() {
        //Arrange
        UserChangePasswordDto changeDto = createUserChangePasswordDto();
        var userDetails = createSpringSecurityUser();

        when(mockUserDetailsManager.loadUserByUsername(USER_USERNAME_TEST))
                .thenReturn(userDetails);

        when(mockPasswordEncoder.matches(changeDto.getOldPassword(), userDetails.getPassword()))
                .thenReturn(true);

        when(mockPasswordEncoder.encode(changeDto.getNewPassword())).thenReturn(changeDto.getNewPassword());

        //Act
        mockUserAuthorityService.changeUserPassword(changeDto, USER_USERNAME_TEST);

        //Assert
        verify(mockUserDetailsManager, times(1))
                .changePassword(userDetails.getPassword(), changeDto.getNewPassword());
    }
}