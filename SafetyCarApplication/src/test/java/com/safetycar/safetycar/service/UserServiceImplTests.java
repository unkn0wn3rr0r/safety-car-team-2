package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.EntityDuplicatedException;
import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.exceptions.UnauthorizedRequestException;
import com.safetycar.safetycar.models.entities.UserDetails;
import com.safetycar.safetycar.repositories.contracts.GenericRepositoryHelper;
import com.safetycar.safetycar.repositories.contracts.UserRepository;
import com.safetycar.safetycar.services.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.safetycar.safetycar.helpers.UserDetailsFactory.*;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    GenericRepositoryHelper<UserDetails> userDetailsRepository;

    @Test
    public void getAll_shouldNotReturnUsers_whenCollection_isEmpty() {
        // Arrange
        when(userDetailsRepository.getAll(UserDetails.class))
                .thenReturn(new ArrayList<>());

        // Act
        var allUsers = userService.getAll();

        // Assert
        assertEquals(0, allUsers.size());
        assertTrue(allUsers.isEmpty());

        verify(userDetailsRepository, times(1)).getAll(UserDetails.class);
    }

    @Test
    public void getAll_shouldReturnUsers_whenCollection_isNotEmpty() {
        // Arrange
        when(userDetailsRepository.getAll(UserDetails.class))
                .thenReturn(of(createUserWithoutId()));

        // Act
        var allUsers = userService.getAll();

        // Assert
        assertFalse(allUsers.isEmpty());
        assertEquals(1, allUsers.size());

        verify(userDetailsRepository, times(1)).getAll(UserDetails.class);
    }

    @Test
    public void getById_shouldThrow_whenUserById_doesNotExist() {
        // Arrange
        when(userDetailsRepository.getById(UserDetails.class, USER_INVALID_ID_TEST))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userService.getById(USER_INVALID_ID_TEST));

        verify(userDetailsRepository, times(1))
                .getById(UserDetails.class, USER_INVALID_ID_TEST);
    }

    @Test
    public void getById_shouldReturnNull_whenUserById_doesNotExist() {
        // Arrange
        when(userDetailsRepository.getById(UserDetails.class, USER_INVALID_ID_TEST))
                .thenReturn(null);

        // Act
        var userById = userService.getById(USER_INVALID_ID_TEST);

        // Assert
        assertNull(userById);

        verify(userDetailsRepository, times(1))
                .getById(UserDetails.class, USER_INVALID_ID_TEST);
    }

    @Test
    public void getById_shouldReturnUser_whenUserById_exists() {
        // Arrange
        when(userDetailsRepository.getById(UserDetails.class, USER_VALID_ID_TEST))
                .thenReturn(createUserWithId());

        // Act
        var userById = userService.getById(USER_VALID_ID_TEST);

        // Assert
        assertNotNull(userById);
        assertEquals(userById, createUserWithId());

        verify(userDetailsRepository, times(1))
                .getById(UserDetails.class, USER_VALID_ID_TEST);
    }

    @Test
    public void create_shouldThrow_whenUsername_alreadyExists() {
        // Arrange
        when(mockUserRepository.existsByUsername(USER_USERNAME_TEST))
                .thenReturn(true);

        // Act Assert
        assertThrows(EntityDuplicatedException.class,
                () -> userService.create(createUserWithoutId()));

        verify(mockUserRepository, times(1))
                .existsByUsername(USER_USERNAME_TEST);
    }

    @Test
    public void create_shouldThrow_whenEmail_alreadyExists() {
        // Arrange
        when(mockUserRepository.existsByEmail(USER_EMAIL_TEST))
                .thenReturn(true);

        // Act Assert
        assertThrows(EntityDuplicatedException.class,
                () -> userService.create(createUserWithoutId()));

        verify(mockUserRepository, times(1))
                .existsByEmail(USER_EMAIL_TEST);
    }

    @Test
    public void createShould_returnUser_whenValid() {
        // Arrange
        var userToCreate = createUserWithoutId();

        when(mockUserRepository.create(userToCreate))
                .thenReturn(userToCreate);

        // Act
        var createdUser = userService.create(userToCreate);

        // Assert
        assertNotNull(createdUser);
    }

    @Test
    public void update_shouldThrow_whenUser_notAuthenticated() {
        // Arrange Act Assert
        assertThrows(UnauthorizedRequestException.class,
                () -> userService.update(createUserWithId(), ""));

        verify(mockUserRepository, times(0))
                .update(createUserWithId());
    }

    @Test
    public void delete_shouldThrow_whenUser_notAuthenticated() {
        // Arrange Act Assert
        assertThrows(UnauthorizedRequestException.class,
                () -> userService.delete(createUserWithId(), "", false));

        verify(mockUserRepository, times(0))
                .update(createUserWithId());
    }

    @Test
    public void deleteShould_setEnable_toFalse() {
        // Arrange
        var userToDelete = createUserWithId();

        when(mockUserRepository.update(userToDelete))
                .thenReturn(userToDelete);

        // Act
        var deletedUser = userService.delete(userToDelete, USER_USERNAME_TEST, true);

        // Assert
        assertFalse(deletedUser.getEnabled());
    }

    @Test
    public void findByUsername_shouldThrow_whenUsername_isNotFound() {
        // Arrange
        when(mockUserRepository.findByUsername(USER_USERNAME_TEST))
                .thenThrow(EntityNotFoundException.class);

        // Act Assert
        assertThrows(EntityNotFoundException.class,
                () -> userService.findByUsername(USER_USERNAME_TEST));

        verify(mockUserRepository, times(1))
                .findByUsername(USER_USERNAME_TEST);
    }

    @Test
    public void updateShould_returnUser_whenValid() {
        // Arrange Act
        var userToUpdate = createUserWithId();

        when(userDetailsRepository.getById(UserDetails.class, USER_VALID_ID_TEST))
                .thenReturn(userToUpdate);

        var userById = userService.getById(USER_VALID_ID_TEST);

        when(mockUserRepository.update(userById))
                .thenReturn(userById);

        var updatedUser = userService.update(userToUpdate, USER_USERNAME_TEST);

        // Assert
        assertNotNull(updatedUser);

        verify(userDetailsRepository, times(2))
                .getById(UserDetails.class, USER_VALID_ID_TEST);
    }
}